package scripts.xMercher;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.ListModel;

import org.tribot.api.General;
import org.tribot.util.Util;

import scripts.xMercher.Utils.Merch;
import scripts.xMercher.Utils.MerchCondition;
import scripts.xMercher.Utils.MerchCondition.MERCH_CONDITION_TYPES;
import scripts.xMercher.Utils.MerchCondition.MERCH_CONDITION_UNITS;
import scripts.xMercher.Utils.MerchManager;
import scripts.xMercher.Utils.MerchProfile;
import scripts.xMercher.Utils.Vars;

public class GUI extends javax.swing.JFrame {

	private boolean savedProfile = false;
	private ArrayList<File> removeFiles = new ArrayList<File>();
	private HashMap<String, Integer> buyLimits = new HashMap<String, Integer>();
    /**
     * Creates new form GUI
     */
    public GUI() {
        initComponents();
    }
                    
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        generalPane = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        stopAfterMinsSpinner = new javax.swing.JSpinner();
        jLabel4 = new javax.swing.JLabel();
        stopAfterMerchsSpinner = new javax.swing.JSpinner();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        loginEveryMinsSpinner = new javax.swing.JSpinner();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        profileLoadCB = new javax.swing.JComboBox();
        loadProfileButton = new javax.swing.JButton();
        deleteProfileButton = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        profileNameField = new javax.swing.JTextField();
        saveProfileButton = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        maxMerchSpinner = new javax.swing.JSpinner();
        jLabel26 = new javax.swing.JLabel();
        itemsPane = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        itemList = new javax.swing.JList();
        removeItemButton = new javax.swing.JButton();
        addItemButton = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        itemNameField = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        buyValueSpinner = new javax.swing.JSpinner();
        jLabel10 = new javax.swing.JLabel();
        sellValueSpinner = new javax.swing.JSpinner();
        jLabel12 = new javax.swing.JLabel();
        percentageSpinner = new javax.swing.JSpinner();
        jLabel13 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        buySellLimitSpinner = new javax.swing.JSpinner();
        jLabel20 = new javax.swing.JLabel();
        amountSpinner = new javax.swing.JSpinner();
        jLabel21 = new javax.swing.JLabel();
        jSpinner1 = new javax.swing.JSpinner();
        jComboBox1 = new javax.swing.JComboBox();
        amountCB = new javax.swing.JComboBox();
        flipCB = new javax.swing.JCheckBox();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        conditionList = new javax.swing.JList();
        addConditionButton = new javax.swing.JButton();
        removeConditionButton = new javax.swing.JButton();
        conditionTypeCB = new javax.swing.JComboBox();
        conditionAmountField = new javax.swing.JSpinner();
        conditionUnitCB = new javax.swing.JComboBox();
        startButton = new javax.swing.JButton();

        setTitle("xMercher - GUI");

        jLabel1.setFont(new java.awt.Font("Ebrima", 0, 48)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(204, 204, 0));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("xMercher");

        jLabel2.setFont(new java.awt.Font("Ebrima", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(204, 204, 0));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("by xCode");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Stopping conditions"));

        jLabel3.setText("Stop script after");

        jLabel4.setText("mins");

        jLabel5.setText("merchs");
        
        itemList.setModel(new DefaultListModel<Merch>());
        profileLoadCB.setModel(new javax.swing.DefaultComboBoxModel(getAllProfiles()));
        conditionList.setModel(new DefaultListModel<MerchCondition>());
        
        DefaultListModel currentList = (DefaultListModel)conditionList.getModel();
        currentList.addElement(new MerchCondition(MERCH_CONDITION_TYPES.REENTER_ITEM_AFTER, 1, MERCH_CONDITION_UNITS.SUCCESSFUL_MERCHES));
        currentList.addElement(new MerchCondition(MERCH_CONDITION_TYPES.REENTER_ITEM_AFTER, 1, MERCH_CONDITION_UNITS.CANCELED_MERCHES));
        currentList.addElement(new MerchCondition(MERCH_CONDITION_TYPES.FLIP_ITEM_AFTER, 1, MERCH_CONDITION_UNITS.CANCELED_MERCHES));
        currentList.addElement(new MerchCondition(MERCH_CONDITION_TYPES.CANCEL_BUY_AFTER, 15, MERCH_CONDITION_UNITS.MINUTES));
        conditionList.setModel(currentList);
        conditionList.invalidate();
        
        File buyLimits = new File(Util.getWorkingDirectory() + File.separator + "xMercher" + File.separator + "buylimits.txt");
        if(buyLimits.exists() && !buyLimits.isDirectory())
        {
        	try (BufferedReader br = new BufferedReader(new FileReader(buyLimits))) {
        	    String line;
        	    while ((line = br.readLine()) != null) {
        	      String[] kv = line.split("_");
        	      if(kv.length == 2)
        	      {
        	    	  this.buyLimits.put(kv[0], Integer.parseInt(kv[1]));
        	      }
        	    }
        	} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }

        jLabel6.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        jLabel6.setText("Keep 0 for no stopping condition.");

        jLabel17.setText("Login every");

        jLabel18.setText("mins");

        jLabel19.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        jLabel19.setText("Keep 0 to stay logged in.");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(stopAfterMerchsSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel5))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(stopAfterMinsSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel4))))
                    .addComponent(jLabel6)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(loginEveryMinsSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel18))
                    .addComponent(jLabel19))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(stopAfterMinsSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(stopAfterMerchsSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(loginEveryMinsSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel19)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Profile"));

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Load"));

        loadProfileButton.setText("Load profile");
        loadProfileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadProfileButtonActionPerformed(evt);
            }
        });

        deleteProfileButton.setText("Delete profile");
        deleteProfileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteProfileButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(profileLoadCB, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(loadProfileButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(deleteProfileButton, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(profileLoadCB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addComponent(loadProfileButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(deleteProfileButton)
                .addGap(12, 12, 12))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Save"));

        saveProfileButton.setText("Save profile");
        saveProfileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveProfileButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(profileNameField)
                    .addComponent(saveProfileButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(profileNameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(saveProfileButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTextPane1.setEditable(false);
        jTextPane1.setBackground(javax.swing.UIManager.getDefaults().getColor("Panel.background"));
        jTextPane1.setBorder(null);
        jTextPane1.setText("Tip: Re-open the GUI when the buy and sell values are set and save a new profile to save a new profile with the set values.");
        jScrollPane3.setViewportView(jTextPane1);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3)
                .addContainerGap())
        );

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jTextArea1.setRows(5);
        jTextArea1.setText("Thank you for using xMercher.\nPlease report if any bugs occur.\nThread URL: https://tribot.org/forums/topic/\n47109-xmercher-ge-flipping-free-beta/");
        jScrollPane1.setViewportView(jTextArea1);

        maxMerchSpinner.setValue(8);

        jLabel26.setText("Max. merches at the same time:");

        javax.swing.GroupLayout generalPaneLayout = new javax.swing.GroupLayout(generalPane);
        generalPane.setLayout(generalPaneLayout);
        generalPaneLayout.setHorizontalGroup(
            generalPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(generalPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(generalPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(generalPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jScrollPane1)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(generalPaneLayout.createSequentialGroup()
                        .addComponent(jLabel26)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(maxMerchSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        generalPaneLayout.setVerticalGroup(
            generalPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(generalPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(generalPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(generalPaneLayout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(generalPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(maxMerchSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel26))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jTabbedPane1.addTab("General", generalPane);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Current items"));

        itemList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                itemListMouseClicked(evt);
            }
        });
        itemList.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemListKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(itemList);

        removeItemButton.setText("Remove item");
        removeItemButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeItemButtonActionPerformed(evt);
            }
        });

        addItemButton.setText("Add item");
        addItemButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addItemButtonActionPerformed(evt);
            }
        });

        jLabel11.setText("Use your arrow keys to change the order");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(removeItemButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(addItemButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(37, 37, 37))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(removeItemButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addItemButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel11)
                .addGap(36, 36, 36))
        );

        jLabel8.setText("Item name:");
        
        itemNameField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                itemNameFieldFocusLost(evt);
            }
        });

        jLabel9.setText("Buy value:");

        jLabel10.setText("Sell value:");

        jLabel12.setText("Flip with percentage of:");

        percentageSpinner.setMinimumSize(new java.awt.Dimension(30, 20));
        percentageSpinner.setPreferredSize(new java.awt.Dimension(30, 20));
        percentageSpinner.setValue(15);

        jLabel13.setText("%");

        jLabel7.setText("GE limit:");

        jLabel20.setText("Amount:");

        jLabel21.setText("Undercut/Overcut margin:");

        jSpinner1.setValue(5);

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Percentage", "Absolute" }));

        amountCB.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Absolute", "Percentage" }));

        flipCB.setSelected(true);
        flipCB.setText("Flip");
        flipCB.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                flipCBStateChanged(evt);
            }
        });

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder("Item conditions"));

        jScrollPane4.setViewportView(conditionList);

        addConditionButton.setText("Add condition");
        addConditionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addConditionButtonActionPerformed(evt);
            }
        });

        removeConditionButton.setText("Remove condition");
        removeConditionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeConditionButtonActionPerformed(evt);
            }
        });

        conditionTypeCB.setModel(new javax.swing.DefaultComboBoxModel(MERCH_CONDITION_TYPES.values()));

        conditionUnitCB.setModel(new javax.swing.DefaultComboBoxModel(MERCH_CONDITION_UNITS.values()));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(addConditionButton, javax.swing.GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(removeConditionButton, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(conditionTypeCB, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(conditionAmountField)
                    .addComponent(conditionUnitCB, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(conditionTypeCB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(conditionAmountField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(conditionUnitCB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(addConditionButton)
                            .addComponent(removeConditionButton)))
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout itemsPaneLayout = new javax.swing.GroupLayout(itemsPane);
        itemsPane.setLayout(itemsPaneLayout);
        itemsPaneLayout.setHorizontalGroup(
            itemsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(itemsPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(itemsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(itemsPaneLayout.createSequentialGroup()
                        .addGroup(itemsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(itemsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(itemsPaneLayout.createSequentialGroup()
                                    .addGroup(itemsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel9)
                                        .addComponent(jLabel8)
                                        .addComponent(jLabel10)
                                        .addComponent(jLabel20))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(itemsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(buyValueSpinner, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
                                        .addComponent(amountSpinner, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(itemNameField, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(sellValueSpinner)))
                                .addGroup(itemsPaneLayout.createSequentialGroup()
                                    .addComponent(jLabel12)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(percentageSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(itemsPaneLayout.createSequentialGroup()
                                    .addComponent(jLabel21)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(itemsPaneLayout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(buySellLimitSpinner)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(itemsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(itemsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(itemsPaneLayout.createSequentialGroup()
                                    .addGap(0, 3, Short.MAX_VALUE)
                                    .addComponent(amountCB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(itemsPaneLayout.createSequentialGroup()
                                    .addComponent(flipCB)
                                    .addGap(0, 0, Short.MAX_VALUE)))
                            .addComponent(jLabel13)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        itemsPaneLayout.setVerticalGroup(
            itemsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(itemsPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(itemsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(itemsPaneLayout.createSequentialGroup()
                        .addGroup(itemsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(itemNameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(itemsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(amountSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel20)
                            .addComponent(amountCB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(itemsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(itemsPaneLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(itemsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel9)
                                    .addComponent(buyValueSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(itemsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel10)
                                    .addComponent(sellValueSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(itemsPaneLayout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addComponent(flipCB)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(itemsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(percentageSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13)
                            .addComponent(jLabel12))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(itemsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel21))
                        .addGap(7, 7, 7)
                        .addGroup(itemsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(buySellLimitSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 55, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Items", itemsPane);

        buyValueSpinner.setValue(0);
    	buyValueSpinner.setEnabled(false);
    	sellValueSpinner.setValue(0);
    	sellValueSpinner.setEnabled(false);
    	percentageSpinner.setEnabled(false);
    	jSpinner1.setEnabled(false);
    	
        startButton.setText("Start merching!");
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 546, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(startButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(startButton)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>                        

    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {                                            
        if(!this.savedProfile)
        {
        	int dialogResult = JOptionPane.showConfirmDialog (this, "You haven't saved your current profile.\r\nAre you sure you want to start the script without saving?","Are you sure?", 0);
        	if(dialogResult != JOptionPane.YES_OPTION){
        		return;
        	}
        }
    	Vars.stopScriptAfterMS = Long.parseLong(stopAfterMinsSpinner.getValue().toString()) * 60000;
        Vars.stopScriptAfterMerchs = Integer.parseInt(stopAfterMerchsSpinner.getValue().toString());
        Vars.nextLoginStep = (long)(Long.parseLong(loginEveryMinsSpinner.getValue().toString()) * 60000 * General.randomDouble(0.95, 1.05));
        Vars.merchAtATime = Integer.parseInt(maxMerchSpinner.getValue().toString());
        ListModel listModel = itemList.getModel();
        Merch[] merchables = new Merch[listModel.getSize()];
        for(int i = 0 ; i < merchables.length ; i++)
        {
        	merchables[i] = (Merch)listModel.getElementAt(i);
        }
        if(merchables.length > 0)
        {
        	Vars.merchManager.clearMerchs();
        	Vars.merchManager.addMerchs(merchables);
        	Vars.guiwait = false;
	        this.setVisible(false);
        }
        else
        {
        	General.println("You have to insert at least one item.");
        }
    }                                           

    public void setStartButtonText(String text)
    {
    	this.startButton.setText(text);
    }
    
    private void loadProfileButtonActionPerformed(java.awt.event.ActionEvent evt) {                                                  
        MerchProfile profile = (MerchProfile)profileLoadCB.getSelectedItem();
       // General.println(profile.getMerchables()[0].getAmount());
        if(profile != null)
        {
        	stopAfterMinsSpinner.setValue(Integer.parseInt(profile.getStopScriptAfterMins()));
        	stopAfterMinsSpinner.invalidate();
        	
        	stopAfterMerchsSpinner.setValue(Integer.parseInt(profile.getStopScriptAfterMerchs()));
        	stopAfterMerchsSpinner.invalidate();
        	
        	loginEveryMinsSpinner.setValue(Integer.parseInt(profile.getLoginEveryMins()));
        	loginEveryMinsSpinner.invalidate();
        	
        	maxMerchSpinner.setValue(Integer.parseInt(profile.getMaxMerchs()));
        	maxMerchSpinner.invalidate();
        	
        	DefaultListModel<Merch> model = new DefaultListModel<Merch>();
            for(Merch p : profile.getMerchables()){
                model.addElement(p);
            }
        	itemList.setModel(model);
        	itemList.invalidate();
        	General.println("Done loading profile");
        	this.savedProfile = true;
        	JOptionPane.showMessageDialog(this,
        		    "Profile loaded.");
        }
    }                                                 

    private void deleteProfileButtonActionPerformed(java.awt.event.ActionEvent evt) {                                                    
        MerchProfile profile = (MerchProfile)profileLoadCB.getSelectedItem();
    	if(profile != null)
    	{
    		File file = new File(Util.getWorkingDirectory() + File.separator + "xMercher" + File.separator + "Profiles", profile.toString() + ".DAT");
    		if(file.delete())
    		{
    			profileLoadCB.setModel(new javax.swing.DefaultComboBoxModel(getAllProfiles()));
    			JOptionPane.showMessageDialog(this,
            		    "Profile removed.");
    		}
    		
    	}
    }                                         

    private void saveProfileButtonActionPerformed(java.awt.event.ActionEvent evt) {                                                  
    	try {
    		File file = new File(Util.getWorkingDirectory() + File.separator + "xMercher" + File.separator + "Profiles", profileNameField.getText() + ".DAT");
    	        FileOutputStream f = new FileOutputStream(file);
    	        ObjectOutputStream s = new ObjectOutputStream(f);

    	        ListModel listModel = itemList.getModel();
    	        Merch[] merchables = new Merch[listModel.getSize()];
    	        for(int i = 0 ; i < merchables.length ; i++)
    	        {
    	        	merchables[i] = (Merch)listModel.getElementAt(i);
    	        }
    	        MerchManager mm = new MerchManager();
    	        mm.addMerchs(merchables);
    	        MerchProfile profile = new MerchProfile(profileNameField.getText(), mm, stopAfterMinsSpinner.getValue().toString(), stopAfterMerchsSpinner.getValue().toString(), loginEveryMinsSpinner.getValue().toString(), maxMerchSpinner.getValue().toString());
    	        s.writeObject(profile);
    	        s.close();
    	        
    	        profileLoadCB.setModel(new javax.swing.DefaultComboBoxModel(getAllProfiles()));
    	        profileLoadCB.invalidate();
    	        JOptionPane.showMessageDialog(this,
            		    "Profile saved!");
    	        this.savedProfile = true;
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    			General.println("Something went wrong saving the profile.");
    		} //creates, overwrites
    }                                                 

    private File[] listProfiles() {
    	File file = new File(Util.getWorkingDirectory() + File.separator + "xMercher" + File.separator + "Profiles");
    	return file.listFiles() != null ? file.listFiles() : new File[]{};
    }
    
    private MerchProfile[] getAllProfiles()
    {
    	File[] profileFiles = listProfiles();
    	MerchProfile[] profiles = new MerchProfile[profileFiles.length];
    	for(int i = 0 ; i < profiles.length ; i++)
    	{
    		profiles[i] = getProfileByFile(profileFiles[i]);
    	}
    	if(removeFiles.size() > 0)
    	{
    		for(File f : removeFiles)
    		{
    			f.delete();
    		}
    		//JOptionPane.showMessageDialog(null,"One or more of your profile-files in the .tribot/xMercher/profiles/ folder is corrupted or out of date. \r\nTherefore, they have been deleted.","Loading profiles error",JOptionPane.WARNING_MESSAGE);
    		//return getAllProfiles();
    	}
    	return profiles;
    }
    
    private MerchProfile getProfileByFile(File file)
    {
    	try
    	{
			FileInputStream f = new FileInputStream(file);
		    ObjectInputStream s = new ObjectInputStream(f);
		    MerchProfile prof = (MerchProfile) s.readObject();
		    s.close();
		    f.close();
		    return prof;
    	} catch (Exception e)
		{
    		removeFiles.add(file);
			e.printStackTrace();
		} 
    	return null;
    }
    
    private void addItemButtonActionPerformed(java.awt.event.ActionEvent evt) {                                              
        Merch itemMerch;
        
        ListModel listModel = conditionList.getModel();
        MerchCondition[] conditions = new MerchCondition[listModel.getSize()];
        for(int i = 0 ; i < conditions.length ; i++)
        {
        	conditions[i] = (MerchCondition)listModel.getElementAt(i);
        }
        
        if(Integer.parseInt(buyValueSpinner.getValue().toString()) <= 0 && Integer.parseInt(sellValueSpinner.getValue().toString()) <= 0)
        {
        	itemMerch = new Merch(itemNameField.getText(), Integer.parseInt(amountSpinner.getValue().toString()), Integer.parseInt(percentageSpinner.getValue().toString()), Integer.parseInt(jSpinner1.getValue().toString()), jComboBox1.getSelectedIndex() == 0, conditions);
        }
        else
        {
        	itemMerch = new Merch(itemNameField.getText(), Integer.parseInt(amountSpinner.getValue().toString()), Integer.parseInt(buyValueSpinner.getValue().toString()), Integer.parseInt(sellValueSpinner.getValue().toString()), conditions);
        }
        
        itemMerch.setExchangeLimit(Integer.parseInt(buySellLimitSpinner.getValue().toString()));
        itemMerch.setAmountAbsolute(amountCB.getSelectedIndex() == 0);
        DefaultListModel currentList = (DefaultListModel)itemList.getModel();
        currentList.addElement(itemMerch);
        itemList.setModel(currentList);
        itemList.invalidate();
    }                                             

    private void removeItemButtonActionPerformed(java.awt.event.ActionEvent evt) {                                                 
        if(itemList.getSelectedIndex() != -1)
    	{
	    	DefaultListModel currentList = (DefaultListModel)itemList.getModel();
	    	currentList.remove(itemList.getSelectedIndex());
	    	itemList.setModel(currentList);
	    	itemList.invalidate();
    	}
    }                                                

    private void itemListMouseClicked(java.awt.event.MouseEvent evt) {                                      
        if(evt.getClickCount() == 2)
        {
        	DefaultListModel currentList = (DefaultListModel)itemList.getModel();
        	if(itemList.getSelectedIndex() != -1)
        	{
        		Merch m = (Merch)currentList.get(itemList.getSelectedIndex());
        		itemNameField.setText(m.getItemName());
        		amountSpinner.setValue(m.getAmount());
        		buyValueSpinner.setValue(m.getBuyValue());
        		sellValueSpinner.setValue(m.getSellValue());
        		buySellLimitSpinner.setValue(m.getExchangeLimit());
        		jSpinner1.setValue(m.getUndercutMargin());
        		flipCB.setSelected(buyValueSpinner.getValue().equals(0) && sellValueSpinner.getValue().equals(0));
        		jComboBox1.setSelectedIndex(m.getUndercutPercentage() ? 0 : 1);
        		amountCB.setSelectedIndex(m.getAmountAbsolute() ? 0 : 1);
        		DefaultListModel<MerchCondition> defModel = new DefaultListModel<MerchCondition>();
        		for(MerchCondition c : m.getConditions())
        		{
        			defModel.addElement(c);
        		}
        		conditionList.setModel(defModel);
        		conditionList.invalidate();
        	}
        }
    }     
    
    private void itemListKeyPressed(java.awt.event.KeyEvent evt) {       
    	DefaultListModel currentList = (DefaultListModel)itemList.getModel();
    	int keyCode = evt.getKeyCode();
    	int selectedIndex = itemList.getSelectedIndex();
    	if(selectedIndex < 0)
    		return;
        switch( keyCode ) { 
            case KeyEvent.VK_UP:
            	if(selectedIndex == 0)
            		break;
                Object temp = currentList.get(selectedIndex);
                currentList.set(selectedIndex, currentList.get(selectedIndex - 1));
                currentList.set(selectedIndex - 1, temp);
                itemList.setModel(currentList);
                break;
            case KeyEvent.VK_DOWN:
            	if(selectedIndex == currentList.getSize() - 1)
            		break;
            	temp = currentList.get(selectedIndex);
                currentList.set(selectedIndex, currentList.get(selectedIndex + 1));
                currentList.set(selectedIndex + 1, temp);
                itemList.setModel(currentList);
                break;
         }
    }                                                                     

    private void itemNameFieldFocusLost(java.awt.event.FocusEvent evt) {    
    	
        if(buyLimits.size() > 0)
        {
        	General.println("lost1");
        	Iterator it = buyLimits.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                if(pair.getKey().toString().toLowerCase().equals(itemNameField.getText().toLowerCase()))
                {
                	General.println("lost3");
                	this.buySellLimitSpinner.setValue(pair.getValue());
                }
                it.remove(); // avoids a ConcurrentModificationException
            }
        }
    }                                       

    private void flipCBStateChanged(javax.swing.event.ChangeEvent evt) {                                    
        if(flipCB.isSelected())
        {
        	buyValueSpinner.setValue(0);
        	buyValueSpinner.setEnabled(false);
        	sellValueSpinner.setValue(0);
        	sellValueSpinner.setEnabled(false);
        	percentageSpinner.setEnabled(true);
        	jSpinner1.setEnabled(true);
        }
        else
        {
        	buyValueSpinner.setEnabled(true);
        	sellValueSpinner.setEnabled(true);
        	percentageSpinner.setEnabled(false);
        	jSpinner1.setEnabled(false);
        }
    }                                   

    private void addConditionButtonActionPerformed(java.awt.event.ActionEvent evt) {                                                   
        MerchCondition condition = new MerchCondition((MERCH_CONDITION_TYPES)conditionTypeCB.getSelectedItem(), (int)conditionAmountField.getValue(), (MERCH_CONDITION_UNITS)conditionUnitCB.getSelectedItem());
        DefaultListModel currentList = (DefaultListModel)conditionList.getModel();
        currentList.addElement(condition);
        conditionList.setModel(currentList);
        conditionList.invalidate();
    }                                                  

    private void removeConditionButtonActionPerformed(java.awt.event.ActionEvent evt) {                                                      
    	if(conditionList.getSelectedIndex() != -1)
    	{
	    	DefaultListModel currentList = (DefaultListModel)conditionList.getModel();
	    	currentList.remove(conditionList.getSelectedIndex());
	    	conditionList.setModel(currentList);
	    	conditionList.invalidate();
    	}
    }                                                     
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify                     
    private javax.swing.JButton addConditionButton;
    private javax.swing.JButton addItemButton;
    private javax.swing.JComboBox amountCB;
    private javax.swing.JSpinner amountSpinner;
    private javax.swing.JSpinner buySellLimitSpinner;
    private javax.swing.JSpinner buyValueSpinner;
    private javax.swing.JSpinner conditionAmountField;
    private javax.swing.JList conditionList;
    private javax.swing.JComboBox conditionTypeCB;
    private javax.swing.JComboBox conditionUnitCB;
    private javax.swing.JButton deleteProfileButton;
    private javax.swing.JCheckBox flipCB;
    private javax.swing.JPanel generalPane;
    private javax.swing.JList itemList;
    private javax.swing.JTextField itemNameField;
    private javax.swing.JPanel itemsPane;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSpinner jSpinner1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextPane jTextPane1;
    private javax.swing.JButton loadProfileButton;
    private javax.swing.JSpinner loginEveryMinsSpinner;
    private javax.swing.JSpinner maxMerchSpinner;
    private javax.swing.JSpinner percentageSpinner;
    private javax.swing.JComboBox profileLoadCB;
    private javax.swing.JTextField profileNameField;
    private javax.swing.JButton removeConditionButton;
    private javax.swing.JButton removeItemButton;
    private javax.swing.JButton saveProfileButton;
    private javax.swing.JSpinner sellValueSpinner;
    private javax.swing.JButton startButton;
    private javax.swing.JSpinner stopAfterMerchsSpinner;
    private javax.swing.JSpinner stopAfterMinsSpinner;
    // End of variables declaration                   
}