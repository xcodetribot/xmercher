package scripts.xMercher.Utils;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.types.RSInterfaceChild;
import org.tribot.api2007.types.RSInterfaceComponent;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSItem.TYPE;

public class GEOffer {
	public enum STATUS {
		BUY,
		SELL,
		EMPTY
	}
	
	private int itemValue;
	private int itemAmount;
	private String itemName;
	private STATUS status;
	private int childInterface;
	private int overviewStatusBarWidth;
	private int offerStatusBarWidth;
	private static final int ITEMS_COLLECT_CHILD = 22;
	
	public GEOffer(STATUS status, int childInterface, String itemName, int itemAmount, int itemValue)
	{
		this.status = status;
		this.childInterface = childInterface;
		this.itemName = itemName;
		this.itemAmount = itemAmount;
		this.itemValue = itemValue;
		overviewStatusBarWidth = 0;
		offerStatusBarWidth = 0;
	}
	
	public GEOffer(STATUS status)
	{
		this.status = status;
	}
	
	public boolean isUpdated()
	{
		if(GrandExchange.isOverviewScreenOpen())
		{
			RSInterfaceChild offer = Interfaces.get(GrandExchange.GE_PARENT_ID, childInterface);
			if(offer != null && !offer.isHidden())
			{
				RSInterfaceComponent statusBar = offer.getChild(GrandExchange.OFFER_PROGRESS_BAR);
				int currWidth = statusBar.getAbsoluteBounds().width;
				if(offerStatusBarWidth < currWidth)
				{
					offerStatusBarWidth = currWidth;
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean isCompleted()
	{
		if(GrandExchange.isOverviewScreenOpen())
		{
			RSInterfaceChild offer = Interfaces.get(GrandExchange.GE_PARENT_ID, childInterface);
			if(offer != null && !offer.isHidden())
			{
				RSInterfaceComponent statusBar = offer.getChild(GrandExchange.OFFER_PROGRESS_BAR);
				if(statusBar != null)
				{
					int currWidth = statusBar.getAbsoluteBounds().width;
					if(currWidth >= 105 || status == STATUS.EMPTY)
					{
						return true;
					}
				}
			}
		}
		RSInterfaceChild rewardChild = Interfaces.get(GrandExchange.GE_PARENT_ID, GrandExchange.REWARD_CHILD);
		if(GrandExchange.isRetrieveScreenOpen() && rewardChild != null && !rewardChild.isHidden() && rewardChild.getChild(2).getComponentItem() != -1)
			return true;
		return false;
	}
	
	public int retrieveItems()//offerStatusBarWidth = currWidth;
	{
		int setPrice = 0;
		if(GrandExchange.isGEScreenOpen() && isCompleted())
		{
			RSInterfaceChild rewardChild = Interfaces.get(GrandExchange.GE_PARENT_ID, GrandExchange.REWARD_CHILD);
			if(GrandExchange.isOfferScreenOpen() && rewardChild.isHidden())
			{
				Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
				RSInterfaceChild backButton = Interfaces.get(GrandExchange.GE_PARENT_ID, GrandExchange.IN_OFFER_BACK_BUTTON);
				if(backButton.click("Back"))
				{
					Timing.waitCondition(new Condition() {
						
						@Override
						public boolean active() {
							General.sleep(10, 30);
							return GrandExchange.isOverviewScreenOpen();
						}
					}, General.random(1200, 1500));
					
					Vars.lastBusyTime = Timing.currentTimeMillis();
				}
			}
			if(!GrandExchange.isRetrieveScreenOpen())
			{
				if(GrandExchange.isOverviewScreenOpen())
				{
					RSInterfaceChild offerButton = Interfaces.get(GrandExchange.GE_PARENT_ID, childInterface);
					Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
					if(offerButton.click("View offer"))
					{
						Timing.waitCondition(new Condition() {
							
							@Override
							public boolean active() {
								General.sleep(10, 30);
								return GrandExchange.isRetrieveScreenOpen();
							}
						}, General.random(1200, 1500));
						
						Vars.lastBusyTime = Timing.currentTimeMillis();
					}
				}
			}
			rewardChild = Interfaces.get(GrandExchange.GE_PARENT_ID, ITEMS_COLLECT_CHILD);
			if(GrandExchange.isRetrieveScreenOpen() && !rewardChild.isHidden())
			{
				RSInterfaceComponent item1 = rewardChild.getChild(2);
				RSInterfaceComponent item2 = rewardChild.getChild(3);
				RSInterfaceComponent totalAmountComp = Interfaces.get(GrandExchange.GE_PARENT_ID, 21).getChild(1);
				String[] totalAmount = totalAmountComp.getText().split("for a total price of <col=ffb83f>");
				if(totalAmount.length == 2)
				{
					setPrice = GrandExchange.stringToInt(totalAmount[1]);
					
				}
				if(item1.getComponentItem() != -1 && !item1.isHidden())
				{
					String componentName = item1.getComponentName() != null ? item1.getComponentName().toLowerCase() : null;
					if(componentName != null && componentName.contains(itemName.toLowerCase()))
					{
						int lastVal = 0;
						if(Vars.itemBuyAmount.containsKey(itemName))
							lastVal = Vars.itemBuyAmount.get(itemName);
						Vars.itemBuyAmount.put(itemName, lastVal + item1.getComponentStack());
					}

					Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
					if(item1.click("Collect"))
					{
						Timing.waitCondition(new Condition() {
							
							@Override
							public boolean active() {
								General.sleep(10, 30);
								return GrandExchange.isOverviewScreenOpen();
							}
						}, General.random(1500, 1800));
						Vars.lastBusyTime = Timing.currentTimeMillis();
					}
				}
				if(item2.getComponentItem() != -1 && !item2.isHidden())
				{
					String componentName = item2.getComponentName() != null ? item2.getComponentName().toLowerCase() : null;
					if(componentName != null && componentName.contains(itemName.toLowerCase()))
					{
						int lastVal = 0;
						if(Vars.itemBuyAmount.containsKey(itemName))
							lastVal = Vars.itemBuyAmount.get(itemName);
						Vars.itemBuyAmount.put(itemName, lastVal + item2.getComponentStack());
					}
					Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
					if(item2.click("Collect"))
					{
						Timing.waitCondition(new Condition() {
							
							@Override
							public boolean active() {
								General.sleep(10, 30);
								return GrandExchange.isOverviewScreenOpen();
							}
						}, General.random(1500, 1800));
						Vars.lastBusyTime = Timing.currentTimeMillis();
					}
				}
				if(GrandExchange.isOverviewScreenOpen())
				{
					return setPrice;
				}
			}
		}
		return 0;
	}
	
	public boolean cancelOffer()
	{
		if(GrandExchange.isGEScreenOpen() && !isCompleted())
		{
			RSInterfaceChild rewardChild = Interfaces.get(GrandExchange.GE_PARENT_ID, GrandExchange.REWARD_CHILD);
			if(GrandExchange.isOfferScreenOpen() && rewardChild.isHidden())
			{
				RSInterfaceChild backButton = Interfaces.get(GrandExchange.GE_PARENT_ID, GrandExchange.IN_OFFER_BACK_BUTTON);
				Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
				if(backButton.click("Back"))
				{
					Timing.waitCondition(new Condition() {
						
						@Override
						public boolean active() {
							General.sleep(10, 30);
							return GrandExchange.isOverviewScreenOpen();
						}
					}, General.random(1200, 1500));
					Vars.lastBusyTime = Timing.currentTimeMillis();
				}
			}
			if(!GrandExchange.isRetrieveScreenOpen())
			{
				if(GrandExchange.isOverviewScreenOpen())
				{
					RSInterfaceChild offerButton = Interfaces.get(GrandExchange.GE_PARENT_ID, childInterface);
					Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
					if(offerButton.click("View offer"))
					{
						Timing.waitCondition(new Condition() {
							
							@Override
							public boolean active() {
								General.sleep(10, 30);
								return GrandExchange.isRetrieveScreenOpen();
							}
						}, General.random(1200, 1500));
						Vars.lastBusyTime = Timing.currentTimeMillis();
					}
				}
			}
			rewardChild = Interfaces.get(GrandExchange.GE_PARENT_ID, GrandExchange.REWARD_CHILD);
			if(GrandExchange.isRetrieveScreenOpen() && !rewardChild.isHidden())
			{
				RSInterfaceComponent cancelButton = Interfaces.get(GrandExchange.GE_PARENT_ID, 21).getChild(0);
				Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
				if(cancelButton.click("Abort offer"))
				{
					Timing.waitCondition(new Condition() {
						
						@Override
						public boolean active() {
							General.sleep(10, 30);
							return GrandExchange.isOverviewScreenOpen();
						}
					}, General.random(1200, 1500));
					Vars.lastBusyTime = Timing.currentTimeMillis();
				}
				RSInterfaceComponent item1 = rewardChild.getChild(2);
				RSInterfaceComponent item2 = rewardChild.getChild(3);
				General.sleep(200, 300);
				item1 = rewardChild.getChild(2);
				if(item1.getComponentItem() != -1 && !item1.isHidden())
				{
					Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
					if(item1.click("Collect"))
					{
						String componentName = item1.getComponentName() != null ? item1.getComponentName().toLowerCase() : null;
						if(componentName != null && componentName.contains(itemName.toLowerCase()))
						{
							int lastVal = 0;
							if(Vars.itemBuyAmount.containsKey(itemName))
								lastVal = Vars.itemBuyAmount.get(itemName);
							Vars.itemBuyAmount.put(itemName, lastVal + item1.getComponentStack());
						}
						Timing.waitCondition(new Condition() {
							
							@Override
							public boolean active() {
								General.sleep(10, 30);
								return GrandExchange.isOverviewScreenOpen();
							}
						}, General.random(1500, 1800));
						Vars.lastBusyTime = Timing.currentTimeMillis();
					}
				}
				if(item2.getComponentItem() != -1 && !item2.isHidden())
				{
					String componentName = item2.getComponentName() != null ? item2.getComponentName().toLowerCase() : null;
					if(componentName != null && componentName.contains(itemName.toLowerCase()))
					{
						int lastVal = 0;
						if(Vars.itemBuyAmount.containsKey(itemName))
							lastVal = Vars.itemBuyAmount.get(itemName);
						Vars.itemBuyAmount.put(itemName, lastVal + item2.getComponentStack());
					}
					Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
					if(item2.click("Collect"))
					{
						Timing.waitCondition(new Condition() {
							
							@Override
							public boolean active() {
								General.sleep(10, 30);
								return GrandExchange.isOverviewScreenOpen();
							}
						}, General.random(1500, 1800));
						Vars.lastBusyTime = Timing.currentTimeMillis();
					}
				}
				if(GrandExchange.isOverviewScreenOpen())
				{
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public String toString()
	{
		return status + " " + childInterface + " " + itemName + " " + itemValue + " " + itemAmount;
	}
	
	public STATUS getStatus()
	{
		return status;
	}
	
	public int getTotalValue()
	{
		return itemAmount * itemValue;
	}
	
	public int getItemAmount()
	{
		return itemAmount;
	}
	
	public int getItemValue()
	{
		return itemValue;
	}
	
	public String getItemName()
	{
		return itemName;
	}
	
	public int getChildInterface()
	{
		return childInterface;
	}
}
