package scripts.xMercher.Utils;

import java.io.Serializable;

import org.tribot.api.General;
import org.tribot.api.Timing;

public class MerchCondition  implements Serializable{
public enum MERCH_CONDITION_TYPES {
	CANCEL_BUY_AFTER("Cancel if buying takes longer than "),
	CANCEL_SELL_AFTER("Cancel if selling takes longer than "),
	FLIP_ITEM_AFTER("Flip buy/sell-values again after "),
	REENTER_ITEM_AFTER("Re-enter item after ");
	
	private String name;
	
	MERCH_CONDITION_TYPES(String name)
	{
		this.name = name;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
};

public enum MERCH_CONDITION_UNITS {
	MINUTES("Minutes"),
	SUCCESSFUL_MERCHES ("Successful merches"),
	CANCELED_MERCHES("Canceled merches");
	
	private String name;
	
	MERCH_CONDITION_UNITS(String name)
	{
		this.name = name;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
}

	private Merch merch;
	private MERCH_CONDITION_TYPES type;
	private int amount;
	private MERCH_CONDITION_UNITS unit;
	private String name = null;
	
	public MerchCondition(MERCH_CONDITION_TYPES type, int amount, MERCH_CONDITION_UNITS unit)
	{
		this.type = type;
		this.amount = amount;
		this.unit = unit;
	}
	
	public boolean isValid(Merch merch)
	{
		this.merch = merch;
		switch (unit) {
		case MINUTES:
			return Timing.currentTimeMillis() - merch.getPlacedAt() >= amount * 60000 && merch.getPlacedAt() > 0;
		case SUCCESSFUL_MERCHES:
			return merch.getCurrentSuccess() >= amount;
		case CANCELED_MERCHES:
			return merch.getCurrentCancels() >= amount;
		default:
			return false;
		}
	}
	
	public String whenIsValid()
	{
		String s = this.toString() + "\r\n";
		switch (unit) {
		case MINUTES:
			s+= merch.getPlacedAt() > 0 ? "   Time left: " + Timing.msToString((amount * 60000) - (Timing.currentTimeMillis() - merch.getPlacedAt())) + "\r\n" : "Not placed yet\r\n";
			break;
		case SUCCESSFUL_MERCHES:
			s+= "   Successful merches left: " + (amount - merch.getCurrentSuccess()) + "\r\n";
			break;
		case CANCELED_MERCHES:
			s+= "   Canceled merches left: " + (amount - merch.getCurrentCancels()) + "\r\n";
			break;
		}
		return s;
	}
	
	public void execute()
	{
		General.println(merch.getPlacedAt());
		General.println(Timing.currentTimeMillis() - merch.getPlacedAt());
		switch (unit) {
		case MINUTES:
			merch.setPlacedAt(0);
		case SUCCESSFUL_MERCHES:
			merch.setCurrentSuccess(0);
		case CANCELED_MERCHES:
			merch.setCurrentCancels(0);
		}
		
		switch (type) {
		case CANCEL_BUY_AFTER:
			merch.setShouldCancelBuying(true);
			break;
		case CANCEL_SELL_AFTER:
			merch.setShouldCancelSelling(true);
			break;
		case FLIP_ITEM_AFTER:
			merch.setResetAfter(true);
		case REENTER_ITEM_AFTER:
			merch.setReEnterItem(true);
		}
	}
	
	@Override
	public String toString()
	{
		if(name != null)
			return name;
		name = type.toString() + amount + " " + unit;
		return name;
	}
}
