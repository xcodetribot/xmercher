package scripts.xMercher.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Login;
import org.tribot.api2007.types.RSInterfaceChild;
import org.tribot.api2007.types.RSInterfaceComponent;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSItemDefinition;
import org.tribot.util.Util;

import scripts.xMercher.Utils.GEOffer.STATUS;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import sun.tools.jar.Main;

public class Merch implements Serializable {
	public enum STATE {
		DETERMINE_VALUES,
		BUYING,
		SELLING,
		DONE,
		WAITING,
		CANCEL,
		WAITING_BUY_LIMIT
	}
	
	private String itemName;
	private int buyValue;
	private int sellValue;
	private int amount;
	private int profit = 0;
	private int exchangeLimit;
	private long waitTime = 0;
	private int calculatedAmount = -1;
	private long placedAt = 0;
	private boolean amountAbsolute = true;
	private boolean reEnterItem = false;
	private boolean resetAfter = false;
	private int currentSuccess = 0;
	private int currentCancels = 0;
	private int totalSuccess = 0;
	private int totalCancels = 0;
	private boolean cancelled = false;
	private boolean bought = false;
	public ArrayList<String> logs = new ArrayList<String>();
	private int undercutMargin;
	private boolean undercutPercentage;
	private boolean sold = false;
	private int percentageMargin;
	private int priority;
	private STATE state;
	private int currentOfferIndex;
	private boolean shouldCancelBuying = false;
	private boolean shouldCancelSelling = false;
	private MerchCondition[] conditions;
	
	public Merch(String itemName, int amount, int percentageMargin, int undercutMargin, boolean percentage, MerchCondition[] conditions)
	{
		this.itemName = itemName;
		this.state = STATE.WAITING;
		this.amount = amount;
		this.percentageMargin = percentageMargin;
		this.priority = 0;
		this.buyValue = -1;
		this.sellValue = -1;
		this.currentOfferIndex = -1;
		this.undercutMargin = undercutMargin;
		this.undercutPercentage = percentage;
		this.conditions = conditions;
	}
	
	public Merch(String itemName, int amount, int buyValue, int sellValue, MerchCondition[] conditions)
	{
		this.itemName = itemName;
		this.state = STATE.WAITING;
		this.amount = amount;
		this.priority = 0;
		this.percentageMargin = 0;
		this.buyValue = buyValue;
		this.sellValue = sellValue;
		this.undercutMargin = 0;
		this.undercutPercentage = true;
		this.currentOfferIndex = -1;
		this.conditions = conditions;
	}
	
	public STATE getState()
	{
		checkConditions();
		GEOffer currentOffer = GrandExchange.findOffer(itemName);
		if(currentOffer != null)
			this.currentOfferIndex = currentOffer.getChildInterface();
		if(sellValue == -1 || buyValue == -1)
		{
			if(currentOffer == null)
			{
				this.state = STATE.DETERMINE_VALUES;
				this.priority = 1;
			}
			else
			{
				if((currentOffer.getStatus() == STATUS.EMPTY && GEInventory.find(itemName).length == 0 && (waitTime == 0 || Timing.currentTimeMillis() >= waitTime)) || (currentOffer.getStatus() == STATUS.BUY && currentOffer.isCompleted()) ||
				(currentOffer.getStatus() == STATUS.EMPTY && GEInventory.find(itemName).length > 0) || (currentOffer.getStatus() == STATUS.SELL && currentOffer.isCompleted()))
				{
					this.state = STATE.DETERMINE_VALUES;
					this.priority = 2;
				}
				else if((currentOffer.getStatus() == STATUS.BUY || currentOffer.getStatus() == STATUS.SELL) && !currentOffer.isCompleted())
				{
					this.priority = 0;
					this.state = STATE.WAITING;
				}
			}
		}
		else if(Timing.currentTimeMillis() < waitTime && !bought)
		{
			this.priority = 0;
			this.state = STATE.WAITING_BUY_LIMIT;
		}
		else if(!bought && (waitTime == 0 || Timing.currentTimeMillis() >= waitTime) && (currentOffer == null || (currentOffer.getStatus() == STATUS.EMPTY && GEInventory.find(itemName).length == 0 && (waitTime == 0 || Timing.currentTimeMillis() >= waitTime)) || (currentOffer.getStatus() == STATUS.BUY && currentOffer.isCompleted())))
		{
			this.state = STATE.BUYING;
			this.priority = 3;
		}
		else if(!sold && (currentOffer == null || (currentOffer.getStatus() == STATUS.EMPTY && GEInventory.find(itemName).length > 0) || (currentOffer.getStatus() == STATUS.SELL && currentOffer.isCompleted())))
		{
			this.state = STATE.SELLING;
			this.priority = 5;
		}
		else if(currentOffer != null && !currentOffer.isCompleted() && (this.shouldCancelBuying || this.shouldCancelSelling ) )
		{
			this.state = STATE.CANCEL;
			this.priority = 4;
		}
		else if(currentOffer != null && !currentOffer.isCompleted() && currentOffer.getStatus() != STATUS.EMPTY)
		{
			this.state = STATE.WAITING;
			this.priority = 0;
		}
		else
		{
			this.state = STATE.DONE;
			this.priority = -1;
		}
		return this.state;
	}
	
	public int getPriority()
	{
		getState();
		return priority;
	}
	
	public void execute()
	{
		if(Login.getLoginState() != org.tribot.api2007.Login.STATE.INGAME)
			return;
		if(!GrandExchange.isGEScreenOpen())
			GrandExchange.openGrandExchangeScreen();
		GEOffer currentOffer = GrandExchange.getAt(this.currentOfferIndex);
		if(currentOffer != null)
		{
			String n = currentOffer.getItemName();
			if(n == null || !n.equalsIgnoreCase(itemName))
			{
				currentOffer = GrandExchange.findOffer(itemName);
				this.currentOfferIndex = currentOffer != null ? currentOffer.getChildInterface() : -1;
			}
		}
		if((GrandExchange.isBuyOfferScreenOpen() && state != Merch.STATE.DETERMINE_VALUES && state != Merch.STATE.BUYING) || (GrandExchange.isSellOfferScreenOpen() && state != Merch.STATE.DETERMINE_VALUES && state != Merch.STATE.SELLING) || GrandExchange.isCollectScreenOpen())
		{
			RSInterfaceChild backButton = Interfaces.get(GrandExchange.GE_PARENT_ID, GrandExchange.IN_OFFER_BACK_BUTTON);
			Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
			if(backButton.click("Back"))
			{
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						General.sleep(10, 30);
						return GrandExchange.isOverviewScreenOpen();
					}
				}, General.random(1200, 1500));
				Vars.lastBusyTime = Timing.currentTimeMillis();
			}
		}
		switch(getState())
		{
		case DETERMINE_VALUES:
			if(this.sellValue == -1)
			{
				Vars.status = "Determining sell value for " + itemName;
				
				if(currentOffer == null || currentOffer.getStatus() == STATUS.EMPTY)
				{
					if(Vars.itemBuyAmount.containsKey(itemName) ? exchangeLimit - Vars.itemBuyAmount.get(itemName) > 0 : true)
					{
						this.currentOfferIndex = GrandExchange.buyItem(itemName, 100 + percentageMargin, 1, true, true);
						this.placedAt = Timing.currentTimeMillis();
					}
					else
					{
						Vars.println(this, "Buy limit of " + itemName + " reached. We will wait 4 hours until buying again.");
						this.waitTime = Timing.currentTimeMillis() + 14400000;
						Vars.itemBuyAmount.put(itemName, 0);
					}
				}
				else
				{
					if(currentOffer.getStatus() == STATUS.BUY && currentOffer.isCompleted())
					{
						int collected = currentOffer.retrieveItems();
						if(collected != 0)
						{
							placedAt = 0;
							this.shouldCancelBuying = false;
							this.shouldCancelSelling = false;
							sellValue = collected;
							this.currentOfferIndex = -1;
							Vars.println(this, "Sell value " + itemName + " set to " + sellValue);
						}
						else
						{
							Vars.println(this, "Collecting failed. Trying again.");
						}
					}
				}
			}
			//currentOffer = GrandExchange.getAt(this.currentOfferIndex);
			if(this.sellValue != -1 && this.buyValue == -1)
			{
				Vars.status = "Determining buy value for " + itemName;
				RSInterfaceComponent[] item = GEInventory.find(itemName);
				if(currentOffer == null || currentOffer.getStatus() == STATUS.EMPTY)
				{
					if(item.length > 0)
						currentOfferIndex = GrandExchange.sellItem(GEInventory.find(itemName)[0], itemName, 100 - percentageMargin, 1, true, true);
					else
					{
						GEOffer offer = GrandExchange.findOffer(itemName, 1, STATUS.SELL);
						if(offer != null)
						{
							currentOfferIndex = offer.getChildInterface();
							Vars.println(this, "Can't find item in inventory: " + itemName);
						}
					}
				}
				else
				{
					//Vars.status = "Waiting";
					if(currentOffer.getStatus() == STATUS.SELL && currentOffer.isCompleted())
					{
						int collected = currentOffer.retrieveItems();
						if(collected != 0)
						{
							this.shouldCancelBuying = false;
							this.shouldCancelSelling = false;
							buyValue = collected;
							placedAt = 0;
							currentOfferIndex = -1;
							Vars.println(this, "Buy value " + itemName + " set to " + buyValue);
							if(undercutMargin > 0)
							{
								int totalMargin = sellValue - buyValue;
								if(totalMargin > 0)
								{
									int undercutTotal = undercutPercentage ? (int)((double)totalMargin / 100.0 * (double)undercutMargin) : undercutMargin;
									Vars.println(this, "The undercut/overcut margin will be: " + undercutTotal + " GP");
									buyValue += undercutTotal;
									sellValue -= undercutTotal;
									Vars.println(this, "Buy value is now: " + buyValue + " and sell value is now: " + sellValue);
								}
							}
						}
					}
				}
			}
			break;
		case BUYING:
			if(!GrandExchange.isGEScreenOpen())
			{
				GrandExchange.openGrandExchangeScreen();
			}
			if(sellValue - buyValue <= 0)
			{
				Vars.println(this, itemName + " is a really bad item to merch with. Sell value lower than or equal to buy value.");
				bought = true;
				sold = true;
				reEnterItem = false;
				resetAfter = false;
				return;
			}
			
			if(!amountAbsolute)
			{
				calculatedAmount = getAmountByPercentage(amount);
			}
			
			GEOffer curr = GrandExchange.findOffer(itemName, STATUS.BUY);
			if(curr != null)
			{
				this.currentOfferIndex = curr.getChildInterface();
				currentOffer = curr;
			}
			if(currentOffer == null || currentOffer.getStatus() == STATUS.EMPTY)
			{	
				Vars.status = "Buying " + (amountAbsolute ? amount : calculatedAmount) + "x " + itemName;
				int buyAmount = amountAbsolute ? amount : calculatedAmount;
				if(exchangeLimit > 0 && Vars.itemBuyAmount.containsKey(itemName))
				{
					int am = exchangeLimit - Vars.itemBuyAmount.get(itemName);
					if(buyAmount > am)
						buyAmount = am;
				}
				if(buyAmount > 0)
				{
					this.currentOfferIndex = GrandExchange.buyItem(itemName, buyValue, buyAmount, false, true);
					this.placedAt = Timing.currentTimeMillis();
				}
				else
				{
					Vars.println(this, "Buy limit of " + itemName + " reached. We will wait 4 hours until buying again.");
					this.waitTime = Timing.currentTimeMillis() + 14400000;
					Vars.itemBuyAmount.put(itemName, 0);
				}
			}
			else
			{
				//Vars.status = "Waiting";
				if(currentOffer.getStatus() == STATUS.BUY && currentOffer.isCompleted())
				{
					int collected = currentOffer.retrieveItems();
					if(collected != 0)
					{
						bought = true;
						currentOfferIndex = -1;
						placedAt = 0;
						this.shouldCancelBuying = false;
						this.shouldCancelSelling = false;
						Vars.setTotalProfit();
						Vars.println(this, "Successfully bought " + (amountAbsolute ? amount : calculatedAmount) + "x " + itemName + " for " + ((amountAbsolute ? amount : calculatedAmount) * buyValue) + "GP in total.");
						if(exchangeLimit > 0 && (Vars.itemBuyAmount.get(itemName) == null || Vars.itemBuyAmount.get(itemName) >= exchangeLimit))
						{
							Vars.println(this, "Buy limit reached. We will wait 4 hours until buying again.");
							this.waitTime = Timing.currentTimeMillis() + 14400000;
							Vars.itemBuyAmount.put(itemName, 0);
						}
					}
				}
			}
			break;
		case SELLING:
			if(!GrandExchange.isGEScreenOpen())
			{
				GrandExchange.openGrandExchangeScreen();
			}
			Vars.status = "Selling " + (amountAbsolute ? amount : calculatedAmount) + "x " + itemName;
			curr = GrandExchange.findOffer(itemName, STATUS.SELL);
			if(curr != null)
			{
				this.currentOfferIndex = curr.getChildInterface();
				currentOffer = curr;
			}
			RSInterfaceComponent[] item = GEInventory.find(itemName);
			if(currentOffer == null || currentOffer.getStatus() == STATUS.EMPTY)
			{
				if(item.length > 0)
				{
					currentOfferIndex = GrandExchange.sellItem(GEInventory.find(itemName)[0], itemName, sellValue, (amountAbsolute ? amount : calculatedAmount), false, false);
					this.placedAt = Timing.currentTimeMillis();
					this.cancelled = false;
				}
				else
				{
					Vars.println(this, "Can't find item in inventory: " + itemName);
					//sold = true;
					currentOfferIndex = -1;
				}
			}
			else
			{
				//Vars.status = "Waiting";
				if(currentOffer.getStatus() == STATUS.SELL && currentOffer.isCompleted())
				{
					int collected = currentOffer.retrieveItems();
					if(collected != 0)
					{
						sold = true;
						currentOfferIndex = -1;
						Vars.println(this, "Successfully sold " + (amountAbsolute ? amount : calculatedAmount) + "x " + itemName + " for " + ((amountAbsolute ? amount : calculatedAmount) * buyValue) + "GP in total.");
						int profit = (sellValue * (amountAbsolute ? amount : calculatedAmount)) - (buyValue * (amountAbsolute ? amount : calculatedAmount));
						currentSuccess++;
						totalSuccess++;
						placedAt = 0;
						checkConditions();
						
						if(reEnterItem && !cancelled)
						{
							this.bought = false;
							this.sold = false;
						}
						if(reEnterItem && resetAfter)
						{
							Vars.println(this, currentSuccess + "th time we merched " + itemName + ". Now resetting buy/sell values.");
							this.currentSuccess = 0;
							this.buyValue = -1;
							this.sellValue = -1;
						}
						this.profit += profit;
						this.placedAt = 0;
						this.shouldCancelBuying = false;
						this.shouldCancelSelling = false;
						Vars.setTotalProfit();
						Vars.merches++;
						Vars.println(this, "Successfully merched the item '" + itemName + "' " + (amountAbsolute ? amount : calculatedAmount) + "x for a total profit of " + profit);
						playSound();
					}
				}
			}
			break;
		case WAITING:
			Vars.status = "Waiting";
			break;
		case CANCEL:
			Vars.status = "Cancelling " + itemName;
			if(!GrandExchange.isGEScreenOpen())
			{
				GrandExchange.openGrandExchangeScreen();
			}
			else if(this.shouldCancelSelling && currentOffer != null && currentOffer.getStatus() == STATUS.SELL)
			{
				Vars.println(this, "Cancel selling: " + itemName);
				curr = GrandExchange.findOffer(itemName, STATUS.SELL);
				if(curr != null)
				{
					this.currentOfferIndex = curr.getChildInterface();
					currentOffer = curr;
					if(currentOffer.cancelOffer() || GEInventory.find(itemName).length > 0)
					{
						this.shouldCancelBuying = false;
						this.shouldCancelSelling = false;
						this.bought = true;
						this.placedAt = 0;
						this.sellValue = -1;
					}
				}
			}
			else if(this.shouldCancelBuying && currentOffer != null && currentOffer.getStatus() == STATUS.BUY)
			{
				Vars.println(this, "Cancel buying: " + itemName);
				curr = GrandExchange.findOffer(itemName, STATUS.BUY);
				if(curr != null)
				{
					this.currentOfferIndex = curr.getChildInterface();
					currentOffer = curr;
					if(currentOffer.cancelOffer())
					{
						this.shouldCancelBuying = false;
						this.shouldCancelSelling = false;
						if(GEInventory.find(itemName).length > 0)
							bought = true;
						currentCancels++;
						totalCancels++;
						placedAt = 0;
						if(!reEnterItem)
						{
							cancelled = true;
						}
						if(reEnterItem && resetAfter)
						{
							Vars.println(this, currentCancels + "th time we cancelled buying " + itemName + ". Now resetting buy/sell values.");
							this.currentCancels = 0;
							this.buyValue = -1;
							this.sellValue = -1;
						}
					}
				}
			}
			else if(this.shouldCancelBuying && currentOffer != null && currentOffer.getStatus() == STATUS.SELL)
			{
				this.shouldCancelBuying = false;
			}
			else if(this.shouldCancelSelling && currentOffer != null && currentOffer.getStatus() == STATUS.BUY)
			{
				this.shouldCancelSelling = false;
			}
			break;
		case DONE:
			Vars.status = "Done";
			break;
		case WAITING_BUY_LIMIT:
			break;
		default:
			break;
		}
	}
	
	@Override
	public String toString()
	{
		return amount + (amountAbsolute ? "x " : "% ") + itemName;
	}
	
	public MerchCondition[] getConditions()
	{
		return this.conditions;
	}
	
	public void setShouldCancelBuying(boolean cancel)
	{
		this.shouldCancelBuying =  cancel;
	}
	
	public void setShouldCancelSelling(boolean cancel)
	{
		this.shouldCancelSelling =  cancel;
	}
	
	public long getPlacedAt()
	{
		return this.placedAt;
	}
	
	public int getCurrentSuccess()
	{
		return this.currentSuccess;
	}
	
	public int getCurrentCancels()
	{
		return this.currentCancels;
	}
	
	public void setPlacedAt(long time)
	{
		this.placedAt = time;
	}
	
	public void setCurrentSuccess(int amount)
	{
		this.currentSuccess = amount;
	}
	
	public void setCurrentCancels(int amount)
	{
		this.currentCancels = amount;
	}
	
	public int getProfit()
	{
		return profit - getMargin();
	}
	
	public int getMargin()
	{
		int margin = sellValue - buyValue;
		return margin > 0 ? margin : -1;
	}
	
	public int getCurrentOfferIndex()
	{
		return this.currentOfferIndex;
	}
	
	public long getWaitTime()
	{
		return this.waitTime;
	}
	
	public int getExchangeLimit()
	{
		return this.exchangeLimit;
	}
	
	public boolean getResetAfter()
	{
		return this.resetAfter;
	}
	
	public void setResetAfter(boolean reset)
	{
		this.resetAfter =  reset;
	}
	
	public boolean getReEnterItem()
	{
		return reEnterItem;
	}
	
	public void setReEnterItem(boolean reEnter)
	{
		this.reEnterItem = reEnter;
	}
	
	public void setWaitTime(long waitTime)
	{
		this.waitTime = waitTime;
	}
	
	public void setExchangeLimit(int exchangeLimit)
	{
		this.exchangeLimit = exchangeLimit;
	}
	
	public String toCompleteString() {
		return itemName + " [Buy-value: " + buyValue + ", Sell-value: " + sellValue + "] Status: " + getState() + " Offer index: " + (currentOfferIndex == -1 ? "None" : currentOfferIndex - 5);
	}
	
	public String getItemName()
	{
		return itemName;
	}
	
	public void setUndercutMargin(int margin)
	{
		this.undercutMargin = margin;
	}
	
	public void setUndercutPercentage(boolean percentage)
	{
		this.undercutPercentage = percentage;
	}
	
	public int getUndercutMargin()
	{
		return undercutMargin;
	}
	
	public boolean getUndercutPercentage()
	{
		return undercutPercentage;
	}
	
	public int getAmount()
	{
		return amount;
	}
	
	public int getBuyValue()
	{
		return buyValue;
	}
	
	public int getSellValue()
	{
		return sellValue;
	}
	
	public boolean getAmountAbsolute()
	{
		return amountAbsolute;
	}
	
	public void setAmountAbsolute(boolean absolute)
	{
		this.amountAbsolute = absolute;
	}
	
	private void checkConditions()
	{
		if(conditions != null)
		{
			for(MerchCondition condition : conditions)
			{
				if(condition.isValid(this))
					condition.execute();
			}
		}
	}
	
	public static void playSound()
	{
		try {
	         // Open an audio input stream.
			File soundFile = new File(Util.getWorkingDirectory() + File.separator + "xMercher" + File.separator + "cash.wav");
	        AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);
	         // Get a sound clip resource.
	        Clip clip = AudioSystem.getClip();
	         // Open audio clip and load samples from the audio input stream.
	        clip.open(audioIn);
	        clip.start();
	     } catch (UnsupportedAudioFileException e) {
	        e.printStackTrace();
	     } catch (IOException e) {
	    	Vars.println(null, "Can't find audio file to play sound.");
	        e.printStackTrace();
	     } catch (LineUnavailableException e) {
	        e.printStackTrace();
	     }
	}
	
	private int getAmountByPercentage(int amount)
	{
		int concreteAmount = 0;
		if(GrandExchange.isGEScreenOpen())
		{
			RSInterfaceComponent[] money = GEInventory.find("Coins");
			if(money != null && money.length > 0)
			{
				concreteAmount = (int)(money[0].getComponentStack() / 100.0 * amount / buyValue);
			}
		}
		else
		{
			RSItem[] money = Inventory.find("Coins");
			if(money.length > 0)
			{
				concreteAmount = (int)(money[0].getStack() / 100.0 * amount / buyValue);
			}
		}
		return concreteAmount;
	}

	public int getTotalSuccess() {
		return this.totalSuccess;
	}

	public int getTotalCancels() {
		return this.totalCancels;
	}
}
