package scripts.xMercher.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class MerchManager implements Comparator<Merch>, Iterable<Merch>, Serializable{

	private List<Merch> list;
	
	public MerchManager() {
        list = new ArrayList<>();
    }

    public void addMerchs(Merch... Merchs) {
        for (Merch Merch: Merchs) {
            if (!list.contains(Merch)) {
                list.add(Merch);
            }
        }
    }

    public void removeMerch(Merch Merch) {
        if (list.contains(Merch)) {
            list.remove(Merch);
        }
    }

    public void clearMerchs() {
        list.clear();
    }

    public int size() {
        return list.size();
    }

    /*
    Return the highest priority valid Merch.
     */
    public Merch getValidMerch() {
    	List<Merch> sorted = new ArrayList<Merch>();
    	for(Merch m : list)
    		sorted.add(m);
        if (sorted.size() > 0) {
            Collections.sort(sorted, this);
            return sorted.get(0);
        }
        return null;
    }

    /*
    Overridden compare method from the Comparator interface used by the
    Collections sort method to determine what Merch is at the head of the priority.

    Added to the standard compare method is a check to see if each
    Merch validates or not before comparing them.
    If one Merch does not validate and the other does, the Merch that validates
    assumes higher priority.

    Refer to the javadoc for the Comparator interface for an explanation of the
    return values from this method.
     */
    @Override
    public int compare(Merch o1, Merch o2) {
        return (o2 != null ? o2.getPriority() : 0) - (o1 != null ? o1.getPriority() : 0);
    }

	@Override
	public Iterator<Merch> iterator() {
		Iterator<Merch> iMerch = list.iterator();
		return iMerch;
	}

}
