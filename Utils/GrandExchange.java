package scripts.xMercher.Utils;

import java.awt.event.KeyEvent;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.Keyboard;
import org.tribot.api.input.Mouse;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.GameTab.TABS;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSInterface;
import org.tribot.api2007.types.RSInterfaceChild;
import org.tribot.api2007.types.RSInterfaceComponent;
import org.tribot.api2007.types.RSInterfaceMaster;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSTile;

import scripts.xMercher.Utils.GEOffer.STATUS;

public class GrandExchange {
	private final static RSArea GE_AREA = new RSArea(new RSTile(3161, 3493, 0), new RSTile(3168, 3486, 0));
	public final static int 	GE_PARENT_ID = 465,
								OFFER_SCREEN = 24,
								OFFER_STATUS = 16,
								OFFER_ITEM_NAME = 19,
								OFFER_ITEM_VALUE = 20,
								OFFER_ITEM_AMOUNT = 18,
								OFFER_PROGRESS_BAR = 22,
								OFFER_BUY_BUTTON = 0,
								OFFER_SELL_BUTTON = 1,
								IN_OFFER_BACK_BUTTON = 3,
								IN_OFFER_TYPE = 18,
								IN_OFFER_CHOOSE_ITEM = 0,
								IN_OFFER_ITEM_NAME = 25,
								IN_OFFER_AMOUNT = 32,
								IN_OFFER_PRICE_PER_ITEM = 39,
								IN_OFFER_TOTAL_PRICE = 43,
								IN_OFFER_QUANTITY_BUTTON = 49,
								IN_OFFER_PRICE_PER_ITEM_BUTTON = 52,
								IN_OFFER_CONFIRM_BUTTON = 26,
								SEARCH_INTERFACE = 162,
								SEARCH_INTERFACE_CHILD = 38,
								SEARCH_BAR = 33,
								ITEM_VALUE_ID = 26,
								REWARD_CHILD = 22;
	public static int offerIndex = -1;
	
	public static boolean isGEScreenOpen()
	{
		RSInterfaceMaster ge = Interfaces.get(GE_PARENT_ID);
		return ge != null;
	}
	
	public static boolean isOverviewScreenOpen()
	{
		RSInterfaceChild child = Interfaces.get(GE_PARENT_ID, 5);
		return child != null && !child.isHidden();
	}
	
	public static boolean isRetrieveScreenOpen()
	{
		RSInterfaceChild child = Interfaces.get(GE_PARENT_ID, 14);
		return child != null && !child.isHidden();
	}
	
	public static boolean isOfferScreenOpen()
	{
		RSInterfaceChild child = Interfaces.get(GE_PARENT_ID, OFFER_SCREEN);
		return child != null && !child.isHidden();
	}
	
	public static boolean isRewardChildVisible()
	{
		RSInterfaceChild child = Interfaces.get(GrandExchange.GE_PARENT_ID, REWARD_CHILD);
		return child != null && !child.isHidden();
	}
	
	public static boolean collectAll()
	{
		if(isOverviewScreenOpen())
		{
			RSInterfaceChild child = Interfaces.get(GE_PARENT_ID, 5);
			if(child != null)
			{
				RSInterfaceComponent button = child.getChild(0);
				if(button != null && !button.isHidden())
				{
					return button.click("Collect to inventory");
				}
			}
		}
		return false;
	}
	
	public static boolean isBuyOfferScreenOpen()
	{
		RSInterfaceChild child = Interfaces.get(GE_PARENT_ID, OFFER_SCREEN);
		if(child != null && !child.isHidden())
		{
			RSInterfaceComponent type = child.getChild(IN_OFFER_TYPE);
			return type != null && type.getText().startsWith("Buy");
		}
		return false;
	}
	
	public static boolean isSellOfferScreenOpen()
	{
		RSInterfaceChild child = Interfaces.get(GE_PARENT_ID, OFFER_SCREEN);
		if(child != null && !child.isHidden())
		{
			RSInterfaceComponent type = child.getChild(IN_OFFER_TYPE);
			return type != null && type.getText().startsWith("Sell");
		}
		return false;
	}
	
	public static boolean isCollectScreenOpen()
	{
		RSInterfaceChild child = Interfaces.get(GE_PARENT_ID, 20);
		return child != null && !child.isHidden();
	}
	
	public static boolean isAtGrandExchange()
	{
		return GE_AREA.contains(Player.getPosition());
	}
	
	public static GEOffer[] getOffers()
	{
		if(isGEScreenOpen())
		{
			GEOffer[] offers = new GEOffer[8];
			for(int i = 6 ; i < 14 ; i++)
			{
				RSInterfaceChild child = Interfaces.get(GE_PARENT_ID, i);
				if(child != null)
				{
					RSInterfaceComponent statusComponent = child.getChild(OFFER_STATUS);
					if(statusComponent != null)
					{
						STATUS status = STATUS.EMPTY;
						switch(statusComponent.getText())
						{
						case "Empty":
							status = STATUS.EMPTY;
							break;
						case "Buy":
							status = STATUS.BUY;
							break;
						case "Sell":
							status = STATUS.SELL;
							break;
						}
						
						if(status == STATUS.EMPTY)
						{
							offers[i - 6] = new GEOffer(status);
						}
						else
						{
							RSInterfaceComponent itemName = child.getChild(OFFER_ITEM_NAME);
							RSInterfaceComponent itemValue = child.getChild(OFFER_ITEM_VALUE);
							RSInterfaceComponent itemAmount = child.getChild(OFFER_ITEM_AMOUNT);
							if(itemName != null && itemValue != null && itemAmount != null)
							{
								offers[i - 6] = new GEOffer(status, i, itemName.getText(), itemAmount.getComponentStack(), stringToInt(itemValue.getText()));
							}
						}
					}
					
				}
			}
			return offers;
		}
		
		return new GEOffer[]{};
	}
	
	public static int sellItem(final RSInterfaceComponent item, final String itemName, int itemValue, int am, boolean percentage, boolean explicit)
	{
		if(isGEScreenOpen())
		{
			RSInterfaceChild inventoryInterface = Interfaces.get(467, 0);
			if(inventoryInterface == null || inventoryInterface.isHidden())
			{
				Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
				Mouse.clickBox(626, 170, 657, 200, 1);
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						General.sleep(10, 30);
						final RSInterfaceChild i = Interfaces.get(467, 0);
						return i != null &&! i.isHidden();
					}
				}, General.random(1200, 1500));
				Vars.lastBusyTime = Timing.currentTimeMillis();
			}
//			if(!TABS.INVENTORY.isOpen())
//			{
//				Vars.println("Openning inventory app");
//				if(TABS.INVENTORY.open())
//				{
//					Timing.waitCondition(new Condition() {
//						
//						@Override
//						public boolean active() {
//							General.sleep(10, 30);
//							return TABS.INVENTORY.isOpen();
//						}
//					}, General.random(1200, 1500));
//				}
//			}
			int itemAmount = am == 0 ? item.getComponentStack() : am;
			if(isBuyOfferScreenOpen())
			{
				RSInterfaceChild backButton = Interfaces.get(GE_PARENT_ID, IN_OFFER_BACK_BUTTON);
				Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
				if(backButton != null && backButton.click("Back"))
				{
					Timing.waitCondition(new Condition() {
						
						@Override
						public boolean active() {
							General.sleep(10, 30);
							return isOverviewScreenOpen();
						}
					}, General.random(1200, 1500));
					Vars.lastBusyTime = Timing.currentTimeMillis();
				}
			}
			if(isOverviewScreenOpen())
			{
				RSInterfaceChild openOffer = getFirstEmptyOffer();
				if(openOffer != null)
				{
					offerIndex = openOffer.getIndex();
					Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
					if(item.click("Offer"))
					{
						Timing.waitCondition(new Condition() {
							
							@Override
							public boolean active() {
								General.sleep(10, 30);
								return isSellOfferScreenOpen();
							}
						}, General.random(1700, 1800));
						Vars.lastBusyTime = Timing.currentTimeMillis();
					}
				}
				else
				{
					return -1;
				}
			}
			if(isSellOfferScreenOpen())
			{
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						General.sleep(10, 30);
						RSInterfaceChild offerScreen = Interfaces.get(GE_PARENT_ID, OFFER_SCREEN);
						if(offerScreen == null)
							return false;
						RSInterfaceComponent amountComp = offerScreen.getChild(IN_OFFER_AMOUNT);
						RSInterfaceComponent itemValueComp = offerScreen.getChild(IN_OFFER_PRICE_PER_ITEM);
						return amountComp != null && itemValueComp != null && !amountComp.getText().equalsIgnoreCase("") && !itemValueComp.getText().equalsIgnoreCase("");
					}
				}, General.random(800, 1200));
				if(!explicit && !percentage && GEInventory.find(itemName).length > 1)
				{
					RSInterfaceComponent[] items = GEInventory.find(itemName);
					int total = 0;
					for(RSInterfaceComponent invItem : items)
					{
						total += invItem.getComponentStack() == 0 ? 1 : invItem.getComponentStack();
					}
					itemAmount = total;
				}
				RSInterfaceChild offerScreen = Interfaces.get(GE_PARENT_ID, OFFER_SCREEN);
				if(offerScreen == null)
					return -1;
				RSInterfaceComponent amountComp = offerScreen.getChild(IN_OFFER_AMOUNT);
				RSInterfaceComponent itemValueComp = offerScreen.getChild(IN_OFFER_PRICE_PER_ITEM);
				if(percentage)
				{
					int geValue = stringToInt(Interfaces.get(GE_PARENT_ID, ITEM_VALUE_ID).getText());
					double percent = (double)geValue / 100.0;
					itemValue = (int)(percent * itemValue);
				}
				if(stringToInt(amountComp.getText()) != itemAmount && (explicit || (!explicit && !percentage && GEInventory.find(itemName).length > 1)))
				{
					RSInterfaceComponent button = offerScreen.getChild(IN_OFFER_QUANTITY_BUTTON);
					if(button != null)
					{
						if(button.click("Enter quantity"))
						{
							Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
							Timing.waitCondition(new Condition() {
								
								@Override
								public boolean active() {
									General.sleep(10, 30);
									RSInterfaceChild searchText = Interfaces.get(SEARCH_INTERFACE, 32);
									return searchText != null && !searchText.isHidden() && searchText.getText().contains("How many do you");
								}
							}, General.random(1200, 1500));
							Vars.lastBusyTime = Timing.currentTimeMillis();
							RSInterfaceChild searchBar = Interfaces.get(SEARCH_INTERFACE, 32);
							if(searchBar != null)
							{
								if(searchBar != null && !searchBar.isHidden() && searchBar.getText().contains("How many do you"))
								{
									Keyboard.typeSend(itemAmount + "");
									General.sleep(100, 400);
									final int finalAmount = itemAmount;
									Timing.waitCondition(new Condition() {
										
										@Override
										public boolean active() {
											General.sleep(10, 30);
											RSInterfaceChild offscreen = Interfaces.get(GE_PARENT_ID, OFFER_SCREEN);
											if(offscreen == null)
												return false;
											RSInterfaceComponent amc = offscreen.getChild(IN_OFFER_AMOUNT);
											return stringToInt(amc.getText()) == finalAmount;
										}
									}, General.random(800, 1200));
									Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
									Vars.lastBusyTime = Timing.currentTimeMillis();
								}
							}
						}
					}
				}
				if(stringToInt(itemValueComp.getText().replaceAll("[^0-9]+","")) != itemValue)
				{
					RSInterfaceComponent button = offerScreen.getChild(IN_OFFER_PRICE_PER_ITEM_BUTTON);
					if(button != null)
					{
						Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
						if(button.click("Enter price"))
						{
							General.sleep(100, 400);
							Timing.waitCondition(new Condition() {
								
								@Override
								public boolean active() {
									General.sleep(10, 30);
									RSInterfaceChild searchText = Interfaces.get(SEARCH_INTERFACE, 32);
									return searchText != null && !searchText.isHidden() && searchText.getText().contains("Set a price");
								}
							}, General.random(1200, 1500));
							Vars.lastBusyTime = Timing.currentTimeMillis();
							RSInterfaceChild searchBar = Interfaces.get(SEARCH_INTERFACE, 32);
							if(searchBar != null)
							{
								if(searchBar != null && !searchBar.isHidden() && searchBar.getText().contains("Set a price"))
								{
									Keyboard.typeSend(itemValue + "");
									General.sleep(100, 400);
									final int itemVal = itemValue;
									Timing.waitCondition(new Condition() {
										
										@Override
										public boolean active() {
											General.sleep(10, 30);
											RSInterfaceChild offscreen = Interfaces.get(GE_PARENT_ID, OFFER_SCREEN);
											if(offscreen == null)
												return false;
											RSInterfaceComponent amc = offscreen.getChild(IN_OFFER_PRICE_PER_ITEM);
											return amc != null && stringToInt(amc.getText().replaceAll("[^0-9]+","")) == itemVal;
										}
									}, General.random(800, 1200));
									Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
									Vars.lastBusyTime = Timing.currentTimeMillis();
								}
							}
						}
					}
				}
				offerScreen = Interfaces.get(GE_PARENT_ID, OFFER_SCREEN);
				if(offerScreen != null)
				{
					amountComp = offerScreen.getChild(IN_OFFER_AMOUNT);
					itemValueComp = offerScreen.getChild(IN_OFFER_PRICE_PER_ITEM);
					if(amountComp != null && itemValueComp != null && offerScreen.getChild(IN_OFFER_ITEM_NAME).getText().toLowerCase().equalsIgnoreCase(itemName.toLowerCase()) && 
							//stringToInt(amountComp.getText()) == itemAmount && 
							stringToInt(itemValueComp.getText()) == itemValue)
					{
						RSInterfaceChild confirmButton = Interfaces.get(GE_PARENT_ID, IN_OFFER_CONFIRM_BUTTON);
						Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
						if(confirmButton != null && !confirmButton.isHidden() && confirmButton.click("Confirm"))
						{
							General.sleep(100, 400);
							Timing.waitCondition(new Condition() {
								
								@Override
								public boolean active() {
									General.sleep(10, 30);
									return isOverviewScreenOpen();
								}
							}, General.random(1500, 2000));
							Vars.lastBusyTime = Timing.currentTimeMillis();
							if(isOverviewScreenOpen())
								return offerIndex;
						}
					}
				}
			}
		}
		else
		{
			GrandExchange.openGrandExchangeScreen();
		}
		return -1;
	}
	
	public static int buyItem(final String itemName, int itemValue, int itemAmount, boolean percentage, boolean exlicitAmount)
	{
		if(isGEScreenOpen())
		{
			if(isSellOfferScreenOpen())
			{
				RSInterfaceChild backButton = Interfaces.get(GE_PARENT_ID, IN_OFFER_BACK_BUTTON);
				Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
				if(backButton != null && backButton.click("Back"))
				{
					Timing.waitCondition(new Condition() {
						
						@Override
						public boolean active() {
							General.sleep(10, 30);
							return isOverviewScreenOpen();
						}
					}, General.random(1200, 1500));
					Vars.lastBusyTime = Timing.currentTimeMillis();
				}
			}
			if(isOverviewScreenOpen())
			{
				RSInterfaceChild openOffer = getFirstEmptyOffer();
				RSInterfaceComponent[] coins = GEInventory.find("Coins");
				if(coins.length == 0 || coins[0].getComponentStack() < (itemAmount * itemValue))
				{
					Vars.println(null, "Not enough money to buy in inventory.");
					General.sleep(4000, 6000);
					return -1;
				}
				if(openOffer != null)
				{
					offerIndex = openOffer.getIndex();
					RSInterfaceComponent buyButton = openOffer.getChild(OFFER_BUY_BUTTON);
					Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
					if(buyButton != null & buyButton.click("Create Buy offer"))
					{
						General.sleep(100, 400);
						Timing.waitCondition(new Condition() {
							
							@Override
							public boolean active() {
								General.sleep(10, 30);
								return isBuyOfferScreenOpen();
							}
						}, General.random(1200, 1500));
						Vars.lastBusyTime = Timing.currentTimeMillis();
					}
				}
			}
			if(isBuyOfferScreenOpen())
			{
				General.println("Called buy");
				RSInterfaceChild offerScreen = Interfaces.get(GE_PARENT_ID, OFFER_SCREEN);
				if(!offerScreen.getChild(IN_OFFER_ITEM_NAME).getText().equalsIgnoreCase(itemName))
				{
					RSInterfaceChild searchbar = Interfaces.get(SEARCH_INTERFACE, SEARCH_INTERFACE_CHILD);
					if(searchbar == null)
					{
						RSInterfaceComponent chooseItemButton = offerScreen.getChild(IN_OFFER_CHOOSE_ITEM);
						Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
						if(chooseItemButton.click("Choose item"))
						{
							General.println("CHoose itemlsdjflsdf");
							General.sleep(800, 1200);
							Timing.waitCondition(new Condition() {
								
								@Override
								public boolean active() {
									General.random(10, 30);
									RSInterfaceChild child = Interfaces.get(SEARCH_INTERFACE, SEARCH_INTERFACE_CHILD);
									return child != null && !child.isHidden();
								}
							}, General.random(2000, 2200));
							Vars.lastBusyTime = Timing.currentTimeMillis();
						}
					}
					else
					{
						Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
						Timing.waitCondition(new Condition() {
							
							@Override
							public boolean active() {
								General.random(10, 30);
								RSInterfaceChild child = Interfaces.get(SEARCH_INTERFACE, SEARCH_INTERFACE_CHILD);
								return child != null && !child.isHidden();
							}
						}, General.random(1200, 1500));
						Vars.lastBusyTime = Timing.currentTimeMillis();
					}
					searchbar = Interfaces.get(SEARCH_INTERFACE, SEARCH_INTERFACE_CHILD);
					General.println("trying find searchbar");
					if(searchbar != null && !searchbar.isHidden())
					{
						General.println("found hbar");
						RSInterfaceChild currentInput = Interfaces.get(SEARCH_INTERFACE, SEARCH_BAR);
						if(currentInput != null && !currentInput.isHidden())
						{
							String currentText = currentInput.getText().toString();
							if(!currentText.equalsIgnoreCase("<col=000000>What would you like to buy?</col> *"))
							{
								Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
								Keyboard.holdKey(KeyEvent.CHAR_UNDEFINED, KeyEvent.VK_BACK_SPACE, new Condition() {
									
									@Override
									public boolean active() {
										General.sleep(10, 30);
										final RSInterfaceChild currentInput2 = Interfaces.get(SEARCH_INTERFACE, SEARCH_BAR);
										return currentInput2.getText().toString().equalsIgnoreCase("<col=000000>What would you like to buy?</col> *");
									}
								});
								Vars.lastBusyTime = Timing.currentTimeMillis();
							}
							currentInput = Interfaces.get(SEARCH_INTERFACE, SEARCH_BAR);
							RSInterfaceChild results = Interfaces.get(SEARCH_INTERFACE, SEARCH_INTERFACE_CHILD);
							currentText = currentInput.getText().toString();
							if(currentText.equalsIgnoreCase("<col=000000>What would you like to buy?</col> *") || results.getChildren().length <= 1)
							{
								Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
								Keyboard.typeString(fuzifyName(itemName));
								Vars.lastBusyTime = Timing.currentTimeMillis();
								Timing.waitCondition(new Condition() {
									
									@Override
									public boolean active() {
										General.sleep(10, 30);
										RSInterfaceChild child = Interfaces.get(SEARCH_INTERFACE, SEARCH_INTERFACE_CHILD);
										RSInterfaceComponent[] children = child != null && child.getChildren() != null ? child.getChildren() : null;
										return child != null && !child.isHidden() && children != null && children.length >= 3;
									}
								}, General.random(2000, 2500));
							}
						}
						Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
						
						RSInterfaceChild results = Interfaces.get(SEARCH_INTERFACE, SEARCH_INTERFACE_CHILD);
						if(results != null && !results.isHidden())
						{
							RSInterfaceComponent[] children = results.getChildren();
							if(children != null && children.length > 1)
							{
								RSInterfaceComponent itemComponent = null;
								for(int i = 0 ; i < (children.length < 26 ? children.length : 26) ; i++)
								{
									if(children[i].getText().equalsIgnoreCase(itemName))
									{
										itemComponent = children[i - 1];
										break;
									}
								}
								if(itemComponent != null)
								{
									Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
									if(itemComponent.click(""))
									{
										General.sleep(100, 400);
										Timing.waitCondition(new Condition() {
											
											@Override
											public boolean active() {
												RSInterfaceChild offs = Interfaces.get(GE_PARENT_ID, OFFER_SCREEN);
												return offs.getChild(IN_OFFER_ITEM_NAME).getText().equalsIgnoreCase(itemName);
											}
										}, General.random(1200, 1500));
										Vars.lastBusyTime = Timing.currentTimeMillis();
										offerScreen = Interfaces.get(GE_PARENT_ID, OFFER_SCREEN);
									}
								}
							}
						}
					}
				}
				if(offerScreen.getChild(IN_OFFER_ITEM_NAME).getText().equalsIgnoreCase(itemName))
				{
					RSInterfaceComponent amountComp = offerScreen.getChild(IN_OFFER_AMOUNT);
					RSInterfaceComponent itemValueComp = offerScreen.getChild(IN_OFFER_PRICE_PER_ITEM);
					if(percentage)
					{
						int geValue = stringToInt(Interfaces.get(GE_PARENT_ID, ITEM_VALUE_ID).getText());
						double percent = (double)geValue / 100.0;
						itemValue = (int)(percent * itemValue);
					}
					if(!exlicitAmount)
					{
						RSItem[] money = Inventory.find("Coins");
						if(money.length > 0 && money[0].getStack() >= itemValue)
						{
							itemAmount = money[0].getStack() / itemValue;
						}
					}
					if(amountComp != null && stringToInt(amountComp.getText()) != itemAmount)
					{
						RSInterfaceComponent button = offerScreen.getChild(IN_OFFER_QUANTITY_BUTTON);
						if(button != null)
						{
							Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
							if(button.click("Enter quantity"))
							{
								General.sleep(100, 400);
								Timing.waitCondition(new Condition() {
									
									@Override
									public boolean active() {
										General.sleep(10, 30);
										RSInterfaceChild searchText = Interfaces.get(SEARCH_INTERFACE, 32);
										return searchText != null && !searchText.isHidden() && searchText.getText().contains("How many do you");
									}
								}, General.random(1200, 1500));
								Vars.lastBusyTime = Timing.currentTimeMillis();
								RSInterfaceChild searchBar = Interfaces.get(SEARCH_INTERFACE, 32);
								if(searchBar != null)
								{
									if(searchBar != null && !searchBar.isHidden() && searchBar.getText().contains("How many do you"))
									{
										final int amm = itemAmount;
										Keyboard.typeSend(itemAmount + "");
										General.sleep(100, 400);
										Timing.waitCondition(new Condition() {
											
											@Override
											public boolean active() {
												General.sleep(10, 30);
												RSInterfaceChild offscreen = Interfaces.get(GE_PARENT_ID, OFFER_SCREEN);
												if(offscreen == null)
													return false;
												RSInterfaceComponent amc = offscreen.getChild(IN_OFFER_AMOUNT);
												if(amc == null)
													return false;
												return stringToInt(amc.getText()) == amm;
											}
										}, General.random(800, 1200));
										Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
										Vars.lastBusyTime = Timing.currentTimeMillis();
									}
								}
							}
						}
					}
					if(itemValueComp != null && stringToInt(itemValueComp.getText()) != itemValue)
					{
						RSInterfaceComponent button = offerScreen.getChild(IN_OFFER_PRICE_PER_ITEM_BUTTON);
						if(button != null)
						{
							Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
							if(button.click("Enter price"))
							{
								General.sleep(100, 400);
								Timing.waitCondition(new Condition() {
									
									@Override
									public boolean active() {
										General.sleep(10, 30);
										RSInterfaceChild searchText = Interfaces.get(SEARCH_INTERFACE, 32);
										return searchText != null && !searchText.isHidden() && searchText.getText().contains("Set a price");
									}
								}, General.random(1200, 1500));
								Vars.lastBusyTime = Timing.currentTimeMillis();
								RSInterfaceChild searchBar = Interfaces.get(SEARCH_INTERFACE, 32);
								if(searchBar != null)
								{
									if(searchBar != null && !searchBar.isHidden() && searchBar.getText().contains("Set a price"))
									{
										General.println("value: " + itemValue);
										Keyboard.typeSend(itemValue + "");
										General.sleep(100, 400);
										final int itemVal = itemValue;
										Timing.waitCondition(new Condition() {
											
											@Override
											public boolean active() {
												General.sleep(10, 30);
												RSInterfaceChild offscreen = Interfaces.get(GE_PARENT_ID, OFFER_SCREEN);
												if(offscreen == null)
													return false;
												RSInterfaceComponent amc = offscreen.getChild(IN_OFFER_PRICE_PER_ITEM);
												if(amc == null)
													return false;
												return stringToInt(amc.getText().replaceAll("[^0-9]+","")) == itemVal;
											}
										}, General.random(800, 1200));
										Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
										Vars.lastBusyTime = Timing.currentTimeMillis();
									}
								}
							}
						}
					}
					offerScreen = Interfaces.get(GE_PARENT_ID, OFFER_SCREEN);
				}
				offerScreen = Interfaces.get(GE_PARENT_ID, OFFER_SCREEN);
				if(offerScreen != null)
				{
					RSInterfaceComponent amountComp = offerScreen.getChild(IN_OFFER_AMOUNT);
					RSInterfaceComponent itemValueComp = offerScreen.getChild(IN_OFFER_PRICE_PER_ITEM);
					if(amountComp != null && itemValueComp != null && offerScreen.getChild(IN_OFFER_ITEM_NAME).getText().equalsIgnoreCase(itemName) && stringToInt(amountComp.getText()) == itemAmount && stringToInt(itemValueComp.getText().replaceAll("[^0-9]+","")) == itemValue)
					{
						RSInterfaceChild confirmButton = Interfaces.get(GE_PARENT_ID, IN_OFFER_CONFIRM_BUTTON);
						Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
						if(confirmButton != null && 
								!confirmButton.isHidden() && 
								confirmButton.click("Confirm"))
						{
							General.sleep(100, 400);
							Timing.waitCondition(new Condition() {
								
								@Override
								public boolean active() {
									General.sleep(10, 30);
									return isOverviewScreenOpen();
								}
							}, General.random(1500, 2000));
							Vars.lastBusyTime = Timing.currentTimeMillis();
							if(isOverviewScreenOpen())
							{
								return offerIndex;
							}
						}
					}
				}
			}
		}
		else
		{
			GrandExchange.openGrandExchangeScreen();
		}
		return -1;
	}
	
	private static String fuzifyName(String itemName) {
		if(General.random(0, 9) % 2 == 1)
		{
			return itemName;
		}
		int startIndex = General.random(0, itemName.length() / 4);
		int endIndex = General.random( itemName.length() / 2, itemName.length());
		return itemName.substring(startIndex, endIndex);
	}

	public static int stringToInt(String value)
	{
		if(value.length() == 0)
			return -1;
		return Integer.parseInt(value.replaceAll("\\D+",""));
	}
	
	public static boolean walkToGE()
	{
		return WebWalking.walkTo(GE_AREA.getRandomTile(), new Condition() {
			
			@Override
			public boolean active() {
				General.sleep(10, 30);
				return isAtGrandExchange();
			}
		}, General.random(2500, 4000));
	}
	
	public static RSInterfaceChild getFirstEmptyOffer()
	{
		int j = 0;
		if(Vars.merchManager != null)
		{
			for(Merch m : Vars.merchManager)
			{
				if(m.getCurrentOfferIndex() > 0)
					j++;
			}
			if(j > Vars.merchAtATime)
			{
				General.sleep(200, 300);
				return null;
			}
		}
		GEOffer[] offers = getOffers();
		for(int i = 0 ; i < offers.length ; i++)
		{
			GEOffer offr = offers[i];
			if(offr != null)
			{
				STATUS stts = offers[i].getStatus();
				if(stts != null && stts == STATUS.EMPTY)
				{
					return Interfaces.get(GE_PARENT_ID, i + 6);
				}
			}
		}
		return null;
	}
	
	public static GEOffer getAt(int index)
	{
		if(index >= 6 && index < 14)
		{
			GEOffer[] offers = getOffers();
			if(offers.length >= index - 5)
				return offers[index - 6];
		}
		return null;
	}
	
	public static GEOffer findOffer(String itemName, int itemAmount, STATUS status)
	{
		GEOffer[] offers = getOffers();
		for(int i = 0 ; i < offers.length ; i++)
		{
			if(offers[i].getStatus() == status && offers[i].getItemName().equalsIgnoreCase(itemName) && offers[i].getItemAmount() == itemAmount)
			{
				return offers[i];
			}
		}
		return null;
	}
	
	public static GEOffer findOffer(String itemName, STATUS status)
	{
		GEOffer[] offers = getOffers();
		for(int i = 0 ; i < offers.length ; i++)
		{
			String offerName = offers[i].getItemName();
			if(offers[i].getStatus() == status && offerName != null && offerName.equalsIgnoreCase(itemName))
			{
				return offers[i];
			}
		}
		return null;
	}
	
	public static GEOffer findOffer(String itemName)
	{
		GEOffer[] offers = getOffers();
		for(int i = 0 ; i < offers.length ; i++)
		{
			if(offers[i] != null)
			{
				String itemNameI = offers[i].getItemName();
				if(itemNameI != null && itemNameI.equalsIgnoreCase(itemName))
				{
					return offers[i];
				}
			}
		}
		return null;
	}
	
	public static boolean close()
	{
		if(isGEScreenOpen())
		{
			RSInterfaceChild child = Interfaces.get(GE_PARENT_ID, 2);
			if(child != null)
			{
				RSInterfaceComponent comp = child.getChild(11);
				if(comp != null && !comp.isHidden())
				{
					Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
					if(comp.click("Close"))
					{
						Timing.waitCondition(new Condition() {
							
							@Override
							public boolean active() {
								General.sleep(10, 30);
								return !isGEScreenOpen();
							}
						}, General.random(1000, 1500));
						Vars.lastBusyTime = Timing.currentTimeMillis();
						return !isGEScreenOpen();
					}
				}
			}
		}
		return false;
	}
	
	public static boolean openGrandExchangeScreen()
	{
		
		if(!isGEScreenOpen())
		{
			RSNPC[] clerks = NPCs.findNearest("Grand Exchange Clerk");
			if(clerks.length > 0)
			{
				if(!clerks[0].isOnScreen())
				{
					Camera.turnToTile(clerks[0].getPosition());
				}
				if(clerks[0].isOnScreen())
				{
					Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
					if(DynamicClicking.clickRSNPC(clerks[0], "Exchange Grand Exchange Clerk"))
					{
						Timing.waitCondition(new Condition() {
							
							@Override
							public boolean active() {
								General.sleep(10, 30);
								return isGEScreenOpen();
							}
						}, General.random(1500, 2000));
						Vars.lastBusyTime = Timing.currentTimeMillis();
					}
				}
			}
		}
		return isGEScreenOpen();
	}
}
