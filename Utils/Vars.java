package scripts.xMercher.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.util.ABCUtil;
import org.tribot.api2007.Login;
import org.tribot.api2007.Login.STATE;

public class Vars {
	public static int totalProfit = 0;
	public static String status = "Init";
	public static final ABCUtil ANTIBAN = new ABCUtil();
	public static long lastBusyTime = 0;
	public static int merches = 0;
	public static long relogEveryMS = 0;
	public static long stopScriptAfterMS = 0;
	public static long stopScriptAfterMerchs = 0;
	public static long nextLogin = 0;
	public static long nextLoginStep = 0;
	public static HashMap<String, Integer> itemBuyAmount = new HashMap<String, Integer>();
	public static int merchAtATime = 6;
	public static boolean guiwait = true;
	public static MerchManager merchManager;
	public static ArrayList<String> logs = new ArrayList<String>();
	private static DateFormat df = new SimpleDateFormat("HH:mm:ss");
	
	public static boolean canLogout()
	{
		
		if(Login.getLoginState() != STATE.INGAME || nextLoginStep == 0)
		{
			return false;
		}
		if(GrandExchange.isGEScreenOpen())
		{
			for(Merch m : merchManager)
			{
				General.println(m.getBuyValue());
				if(m.getState() == Merch.STATE.WAITING && m.getCurrentOfferIndex() > 0 && m.getBuyValue() > 0 && m.getSellValue() > 0)
					return true;
//				if(m.getState() == Merch.STATE.DETERMINE_VALUES || m.getState() == Merch.STATE.SELLING || m.getState() == Merch.STATE.BUYING)
//				{
//					return false;
//				}
//				if(m.getState() == Merch.STATE.WAITING || m.getState() == Merch.STATE.DONE)
//					return GrandExchange.getFirstEmptyOffer() == null;
			}
			return false;
		}
		return false;
	}
	
	public static void println(Merch m, Object message)
	{
		Date today = Calendar.getInstance().getTime();        
		String reportDate = df.format(today);
		String mes = "[" + reportDate + "] " + message.toString();
		logs.add(mes);
		if(m != null)
		{
			m.logs.add(mes);
		}
		General.println(message.toString());
	}
	
	public static void setTotalProfit()
	{
		int total = 0;
		for(Merch m : merchManager)
		{
			total += m.getProfit();
		}
		Vars.totalProfit = total;
	}
}
