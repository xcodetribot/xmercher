package scripts.xMercher.Utils;

import java.util.ArrayList;

import org.tribot.api.General;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.types.RSInterfaceChild;
import org.tribot.api2007.types.RSInterfaceComponent;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSItem.TYPE;

public class GEInventory {

	private static final int INVENTORY_PARENT = 467;
	
	public static RSInterfaceComponent[] getAll()
	{
		RSInterfaceChild inv = Interfaces.get(INVENTORY_PARENT, 0);
		ArrayList<RSInterfaceComponent> itemList = new ArrayList<RSInterfaceComponent>();
		if(inv != null)
		{
			for(int i = 0 ; i < 28 ; i++)
			{
				RSInterfaceComponent itemComponent = inv.getChild(i);
				if(itemComponent != null && !itemComponent.getComponentName().isEmpty())
				{
					itemList.add(itemComponent);
				}
			}
		}
		return (RSInterfaceComponent[])itemList.toArray(new RSInterfaceComponent[itemList.size()]);
	}
	
	public static RSInterfaceComponent[] find(String name)
	{
		RSInterfaceChild inv = Interfaces.get(INVENTORY_PARENT, 0);
		ArrayList<RSInterfaceComponent> itemList = new ArrayList<RSInterfaceComponent>();
		if(inv != null)
		{
			for(int i = 0 ; i < 28 ; i++)
			{
				RSInterfaceComponent itemComponent = inv.getChild(i);
				if(itemComponent != null && itemComponent.getComponentName().toLowerCase().contains(name.toLowerCase()) && itemComponent.getComponentItem() != 6512)
				{
					//General.println("Found item: " + name.toLowerCase());
					itemList.add(itemComponent);
				}
			}
		}
		RSInterfaceComponent[] returnValue = (RSInterfaceComponent[])itemList.toArray(new RSInterfaceComponent[itemList.size()]);
		return returnValue != null ? returnValue : new RSInterfaceComponent[]{};
	}
}
