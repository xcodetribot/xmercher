package scripts.xMercher.Utils;

import java.io.Serializable;

public class MerchProfile implements Serializable{
	private String profileName;
	private MerchManager merchables;
	private String stopScriptAfterMins;
	private String stopScriptAfterMerchs;
	private String maxMerchs;
	private String loginEveryMins;
	
	public MerchProfile(String name, MerchManager merchables, String stopMins, String stopMerchs, String loginMins, String maxMerchs)
	{
		this.profileName = name;
		this.merchables = merchables;
		this.stopScriptAfterMins = stopMins;
		this.stopScriptAfterMerchs = stopMerchs;
		this.loginEveryMins = loginMins;
		this.maxMerchs = maxMerchs;
	}
	
	@Override
	public String toString()
	{
		return profileName;
	}
	
	public MerchManager getMerchables()
	{
		return merchables;
	}
	
	public String getStopScriptAfterMins()
	{
		return stopScriptAfterMins;
	}
	
	public String getStopScriptAfterMerchs()
	{
		return stopScriptAfterMerchs;
	}
	
	public String getLoginEveryMins()
	{
		return loginEveryMins;
	}
	
	public String getMaxMerchs()
	{
		return maxMerchs;
	}
}
