package scripts.xMercher;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;
import javax.swing.SwingUtilities;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.util.Screenshots;
import org.tribot.api2007.Login;
import org.tribot.api2007.Skills;
import org.tribot.api2007.Login.STATE;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Ending;
import org.tribot.script.interfaces.EventBlockingOverride;
import org.tribot.script.interfaces.MessageListening07;
import org.tribot.script.interfaces.Painting;
import org.tribot.util.Util;

import scripts.xMercher.Nodes.*;
import scripts.xMercher.Utils.GrandExchange;
import scripts.xMercher.Utils.Merch;
import scripts.xMercher.Utils.MerchManager;
import scripts.xMercher.Utils.Vars;

@ScriptManifest(authors = { "xCode" }, category = "Money making", name = "xMercher", version = 1.0)
public class xMercher extends Script implements Painting, EventBlockingOverride, Ending, MessageListening07{

	private final Image paintBG = getImage("http://i.imgur.com/SVsLyKV.png"),
						closeButton = getImage("http://i.imgur.com/BMSnfqq.png"),
						downButton = getImage("http://i.imgur.com/MiLEhRW.png"),
						upButton = getImage("http://i.imgur.com/WQNuprf.png");
	private final static String USER_AGENT = "Mozilla/5.0";
	private final RenderingHints ANTIALIASING = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	private static char[] c = new char[]{'k', 'm', 'b', 't'};
	private boolean paintVisible = true;
	private final Font textFont = new Font("SansSerif", Font.BOLD, 13);
	private final Font smallFont = new Font("SansSerif", Font.PLAIN, 10);
	private final Color textColor = new Color(240, 235, 181);
	private final Color bgColor = new Color(102, 51, 0, 200 );
	private boolean showList = true;
	private GUI gui = new GUI();
	private LogGUI logGUI = new LogGUI();
	private ArrayList<Node> nodes = new ArrayList<Node>();
	
	@Override
	public void run() {
		Vars.merchManager = new MerchManager();
		new File(Util.getWorkingDirectory() + File.separator + "xMercher" + File.separator + "Profiles").mkdirs();
		new File(Util.getWorkingDirectory() + File.separator + "xMercher" + File.separator + "Screenshots").mkdirs();
		new File(Util.getWorkingDirectory() + File.separator + "xMercher" + File.separator + "Logs").mkdirs();
		this.setLoginBotState(false);
		this.setAIAntibanState(false);
		try {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    try {
                    	gui.setVisible(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } 

        while(Vars.guiwait)
        {
	    	sleep(500);
        }
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
                	logGUI.init();
                	logGUI.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
		init();
		while(true)
		{
			for (final Node n : nodes) {
				if (n.isValid()) {
					n.execute();
					General.sleep(30, 100);
				}
			}
		}
	}
	
	private void init()
	{
		nodes.add(new ExecuteMerchs());
		nodes.add(new LoginPlayer());
		nodes.add(new WalkToGE());
		nodes.add(new Antiban());
		nodes.add(new GetMoney());
		nodes.add(new LogoutPlayer());
	}

	@Override
	public void onPaint(Graphics gg) {
		Graphics2D g = (Graphics2D)gg;
		g.setRenderingHints(ANTIALIASING);
		if(!paintVisible)
		{
			g.drawImage(closeButton, 489, 347, null);
			return;
		}
		
		g.drawImage(paintBG, 0, 338, null);
		int height = Vars.merchManager.size() * 13;
		g.setColor(bgColor);
		if(showList)
			g.fillRect(2, 338 - height, 516, height);
		int itemY = 338 - height + 11;
		double profitPH = ((double)Vars.totalProfit * (3600000.0 / this.getRunningTime()));
		g.setFont(textFont);
		g.setColor(textColor);
		g.drawString(Timing.msToString(this.getRunningTime()), 115, 397);
		g.drawString(toMoney((double)Vars.totalProfit, 0), 115, 426);
		g.drawString(toMoney(profitPH, 0), 115, 455);
		
		//Debug
		if(false)
		{
			g.setColor(Color.black);
			g.fillRect(25, 30, 300, 125);
			g.setColor(Color.white);
			g.drawString("Is buy screen open: " + GrandExchange.isBuyOfferScreenOpen() + "", 30, 50);
			g.drawString("Is collect screen open: " + GrandExchange.isCollectScreenOpen() + "", 30, 65);
			g.drawString("Is GE screen open: " + GrandExchange.isGEScreenOpen() + "", 30, 80);
			g.drawString("Is sell screen open: " + GrandExchange.isSellOfferScreenOpen() + "", 30, 95);
			g.drawString("Is overview screen open: " + GrandExchange.isOverviewScreenOpen() + "", 30, 110);
			g.drawString("Is offer screen open: " + GrandExchange.isOfferScreenOpen() + "", 30, 125);
			g.drawString("Is reward child open: " + GrandExchange.isRewardChildVisible() + "", 30, 140);
			g.drawString("Is retrieve screen open: " + GrandExchange.isRetrieveScreenOpen() + "", 30, 155);
		}
		g.drawString(Vars.merches + "", 292, 397);
		g.drawString(getClass().getAnnotation(ScriptManifest.class).version() + "", 292, 426);
		g.drawString(Vars.status, 292, 455);
		
		if(Login.getLoginState() == STATE.LOGINSCREEN && Vars.nextLogin != 0 && Vars.nextLogin >= Timing.currentTimeMillis())
		{
			g.drawString("Waiting for " + Timing.msToString(Vars.nextLogin - Timing.currentTimeMillis()), 313, 198);
		}
		
		g.drawImage(showList ? downButton : upButton, 464, 347, null);

		int y = 195;
		int i = 0;
		for(scripts.xMercher.Utils.Merch m : Vars.merchManager)
		{
			int _x = -1;
			int _y = -1;
			switch(m.getCurrentOfferIndex())
			{
			case 6:
				_x = 70;
				_y = 115;
				break;
			case 7:
				_x = 188;
				_y = 115;
				break;
			case 8:
				_x = 306;
				_y = 115;
				break;
			case 9:
				_x = 424;
				_y = 115;
			case 10:
				_x = 70;
				_y = 235;
				break;
			case 111:
				_x = 188;
				_y = 235;
				break;
			case 12:
				_x = 306;
				_y = 235;
				break;
			case 13:
				_x = 424;
				_y = 235;
				break;
			}
			if(GrandExchange.isOverviewScreenOpen() && _x > 0 && _y > 0 && m.getMargin() > 0)
			{
				g.setColor(bgColor);
				g.fillRect(_x, _y, 68, 43);
				g.setFont(smallFont);
				g.setColor(Color.white);
				double xpPH = ((double)m.getProfit() * (3600000.0 / this.getRunningTime()));
				g.drawString("P:" + toMoney((double)m.getProfit(), 0), _x + 3, _y + 11);
				//g.drawString("M:" + toMoney((double)m.getMargin(), 0), _x + 3, _y + 24);
				g.drawString("S: " + m.getCurrentSuccess() + " C: " + m.getCurrentCancels(), _x + 3, _y + 24);
				g.drawString(toMoney(xpPH, 0) + " P/H", _x + 3, _y + 37);
			}
			if(showList)
			{
				g.setFont(smallFont);
				g.setColor(Color.white);
				g.drawString("- " + m.toCompleteString(), 8, itemY + i * 13);
			}
			i++;
		}
//		int _y = 50;
//		int curr = 0;
//		for (Entry<String, Integer> entry : Vars.itemBuyAmount.entrySet()) {
//		    String key = entry.getKey();
//		    Integer value = entry.getValue();
//		    g.drawString(key + ": " + value.toString(), 20, _y + curr * 15);
//		    curr++;
//		}
	}
	
	private Image getImage(String url) 
	{
		try 
		{
			return ImageIO.read(new URL(url));
		} 
		catch(IOException e) 
		{
			return null;
		}
	}
	
	private static String toMoney(double n, int iteration) {
	    double d = ((long) n / 100) / 10.0;
	    boolean isRound = (d * 10) %10 == 0;
	    return (d < 1000?
	        ((d > 99.9 || isRound || (!isRound && d > 9.99)?
	         (int) d * 10 / 10 : d + ""
	         ) + "" + c[iteration]) 
	        : toMoney(d, iteration+1));

	}

	@Override
	public OVERRIDE_RETURN overrideKeyEvent(KeyEvent arg0) {
		return OVERRIDE_RETURN.PROCESS;
	}

	@Override
	public OVERRIDE_RETURN overrideMouseEvent(MouseEvent e) {
		Point p = e.getPoint();
		if(p.getX() >= 468 && p.getY() >= 394 && p.getX() <= 506 && p.getY() <= 420 && e.getButton() == 1)
		{
			Screenshots.take(true);
			General.println("Screenshot taken!");
			return OVERRIDE_RETURN.DISMISS;
		}
		else if(p.getX() >= 376 && p.getY() >= 393 && p.getX() <= 414 && p.getY() <= 419 && e.getButton() == 1)
		{
			gui.setStartButtonText("Continue merching!");
			gui.setVisible(true);
			return OVERRIDE_RETURN.DISMISS;
		}
		else if(p.getX() >= 490 && p.getY() >= 347 && p.getX() <= 507 && p.getY() <= 366 && e.getButton() == 1)
		{
			paintVisible = !paintVisible;
			return OVERRIDE_RETURN.DISMISS;
		}
		else if(p.getX() >= 465 && p.getY() >= 347 && p.getX() <= 482 && p.getY() <= 366 && e.getButton() == 1)
		{
			showList = !showList;
			return OVERRIDE_RETURN.DISMISS;
		}
		else if(p.getX() >= 423 && p.getY() >= 390 && p.getX() <= 460 && p.getY() <= 420 && e.getButton() == 1)
		{
			logGUI.setVisible(true);
			return OVERRIDE_RETURN.DISMISS;
		}
		return OVERRIDE_RETURN.PROCESS;
	}

	@Override
	public void onEnd() {
		
	}
	
	@Override
	public void clanMessageReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void duelRequestReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void personalMessageReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void playerMessageReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void serverMessageReceived(String msg) {
		if(msg.contains("You haven't got enough"))
		{
			
		}
	}

	@Override
	public void tradeRequestReceived(String arg0) {
		// TODO Auto-generated method stub
		
	}

}
