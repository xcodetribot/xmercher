package scripts.xMercher.Nodes;

import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;

import scripts.xMercher.Utils.GEInventory;
import scripts.xMercher.Utils.GrandExchange;
import scripts.xMercher.Utils.Vars;

public class GetMoney extends Node{

	@Override
	public boolean isValid() {
		return shouldBank();
	}

	@Override
	public void execute() {
		Vars.status = "Banking";
		if(GrandExchange.isGEScreenOpen())
		{
			GrandExchange.close();
		}
		else if(!Banking.isBankScreenOpen())
		{
			Banking.openBank();
		}
		else if(Banking.isBankScreenOpen())
		{
			if(Banking.withdraw(-1, "Coins"))
			{
				Banking.close();
			}
		}
	}
	
	
	public static boolean shouldBank()
	{
		if(GrandExchange.isGEScreenOpen())
			return GEInventory.find("Coins").length == 0 || Banking.isBankScreenOpen();
		else
			return Inventory.find("Coins").length == 0 || Banking.isBankScreenOpen();
	}

}
