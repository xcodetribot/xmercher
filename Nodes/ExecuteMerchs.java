package scripts.xMercher.Nodes;

import org.tribot.api.General;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Login;
import org.tribot.api2007.Login.STATE;

import scripts.xMercher.Utils.GEInventory;
import scripts.xMercher.Utils.Merch;
import scripts.xMercher.Utils.Vars;

public class ExecuteMerchs extends Node{

	@Override
	public boolean isValid() {
		return Login.getLoginState() == STATE.INGAME && !GetMoney.shouldBank();
	}

	@Override
	public void execute() {
		Merch merch = Vars.merchManager.getValidMerch();
		if (merch != null) {
			merch.execute();
		}
	}

}
