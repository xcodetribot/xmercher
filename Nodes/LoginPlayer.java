package scripts.xMercher.Nodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Login;
import org.tribot.api2007.Login.STATE;

import scripts.xMercher.Utils.Merch;
import scripts.xMercher.Utils.Vars;

public class LoginPlayer extends Node{

	@Override
	public boolean isValid() {
		if(Login.getLoginState() != STATE.INGAME)
		{
			if(Vars.nextLogin == 0 || Vars.nextLogin <= Timing.currentTimeMillis())
			{
				if(Vars.nextLogin <= Timing.currentTimeMillis() && Vars.nextLogin > 0)
					General.println("Logging in again, because " + Timing.msToString(Vars.nextLoginStep) + " has passed.");
				return true;
			}
			if(Vars.nextLoginStep == 0)
			{
				for(final Merch merch : Vars.merchManager)
				{
					if(merch.getWaitTime() == 0 || Timing.currentTimeMillis() >= merch.getWaitTime())
					{
						General.println("Logging in again, because the wait time for " + merch.getItemName() + " has passed.");
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public void execute() {
		Vars.status = "Logging in";
		Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, false);
		Login.login();
		Vars.lastBusyTime = Timing.currentTimeMillis();
	}

}
