package scripts.xMercher.Nodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.Mouse;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Login;
import org.tribot.api2007.GameTab.TABS;
import org.tribot.api2007.Login.STATE;

import scripts.xMercher.Utils.GrandExchange;
import scripts.xMercher.Utils.Vars;

public class LogoutPlayer extends Node{

	@Override
	public boolean isValid() {
		return Vars.canLogout();
	}

	@Override
	public void execute() {
		Vars.status = "Logging out";
		if(!GrandExchange.isGEScreenOpen())
		{
			Login.logout();
		}
		else
		{
			TABS.LOGOUT.open();
				Mouse.clickBox(577, 365, 703, 381, 1);
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						General.sleep(10, 30);
						return Login.getLoginState() != STATE.INGAME;
					}
				}, General.random(5000, 6000));
		}
		if(Login.getLoginState() != STATE.INGAME)
		{
			Vars.nextLogin = Timing.currentTimeMillis() + Vars.nextLoginStep;
			General.println("Next login in: " + Timing.msToString(Vars.nextLoginStep));
		}
	}

}
