package scripts.xMercher.Nodes;

import scripts.xMercher.Utils.GrandExchange;
import scripts.xMercher.Utils.Vars;

public class WalkToGE extends Node{

	@Override
	public boolean isValid() {
		return !GrandExchange.isAtGrandExchange();
	}

	@Override
	public void execute() {
		Vars.status = "Walking to GE";
		GrandExchange.walkToGE();
	}

}
