package scripts.xMercher.Nodes;

import scripts.xMercher.Utils.GrandExchange;
import scripts.xMercher.Utils.Vars;

public class Antiban extends Node{

	@Override
	public boolean isValid() {
		return GrandExchange.isGEScreenOpen();
	}

	@Override
	public void execute() {
		Vars.ANTIBAN.performCombatCheck();

		Vars.ANTIBAN.performEquipmentCheck();

		Vars.ANTIBAN.performFriendsCheck();

		Vars.ANTIBAN.performLeaveGame();

		Vars.ANTIBAN.performMusicCheck();

		Vars.ANTIBAN.performPickupMouse();

		Vars.ANTIBAN.performQuestsCheck();

		Vars.ANTIBAN.performRandomMouseMovement();

		Vars.ANTIBAN.performRandomRightClick();

		Vars.ANTIBAN.performRotateCamera();
	}

}
